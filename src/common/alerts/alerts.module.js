(function(module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('alerts', {
            url: '/alerts',
            views: {
                "main": {
                    controller: 'AlertsController as model',
                    templateUrl: 'alerts/alerts.tpl.html'
                }
            },
            data:{ pageTitle: 'Alerts' }
        });
    });

}(angular.module("vrumProject.alerts", [
    'ui.router'
])));
