(function(module) {


	module.controller('AlertsController', ['$scope', 'Endpoint', 'AlertsFactory', AlertsController]);

	module.factory('AlertsFactory', AlertsFactory);
	var message = '';
	// Clase para crear un nuevo dialogo
	function AlertsDialog($modal) {
		var dialog;

		function show(msg) {
			if (msg !== null) {
				message = msg;
			} else {
				message = "Something It's wrong. Please check your email or password.";
			}

			dialog = $modal.open({
				templateUrl: 'alerts/alerts.tpl.html',
				controller: 'AlertsController as model',
				size: 'lg',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	function AlertsFactory($modal) {
		return new AlertsDialog($modal);
	}


	function AlertsController($scope, Endpoint, AlertsFactory) {
		var model = this;
		model.messageShow = '';
		model.dismiss = dismiss;

		window.addEventListener("keydown", moveSomething, false);

		function moveSomething(e) {
			if (e.keyCode === 13 ) {
				model.dismiss();
			}
		}


		function dismiss() {
			AlertsFactory.dismiss();
		}

		function init() {
			model.messageShow = message;
		}
		init();

	}

}(angular.module("vrumProject.alerts")));