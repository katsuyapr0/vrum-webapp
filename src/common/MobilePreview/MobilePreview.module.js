(function(module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('MobilePreview', {
            url: '/mobilepreview',
            views: {
                "main": {
                    controller: 'MobilePreviewController as model',
                    templateUrl: 'MobilePreview/MobilePreview.tpl.html'
                }
            },
            data:{ pageTitle: 'MobilePreview' }
        });
    });

}(angular.module("vrumProject.MobilePreview", [
    'ui.router'
])));
