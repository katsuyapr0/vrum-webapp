(function(module) {

	module.directive('previewDirective', function() {
		return {
			restrict: 'EA',
			controller: 'MobilePreviewController as model',
			templateUrl: 'MobilePreview/MobilePreview.tpl.html',
			scope: {
				type: '@',
				dash:"@",
			},
		};
	});

	module.controller('MobilePreviewController', ["$scope", "$sce", function($scope, $sce) {
		var model = this;
		model.isPDFBool = false;
		model.titlePreview = '';

		init();

		function init() {
			console.log('TYPE DE MBILE PREVIEW', $scope.type);
			model.titlePreview = $scope.type === "mhp" ? "Your Mobile Home Page" : "Your Special";
			model.titlePreview=$scope.dash?'':model.titlePreview;
			var myBroadcast = $scope.type === "mhp" ? "previewDataMhp" : "previewDataSpecial";

			$scope.$on(myBroadcast, function(event, args) {
				console.log("DATOS QQUE LLEGAN PREVIEW", args.fileSrc);
				model.isPDFBool = args.isPDF;
				$scope.imageSrc = model.isPDF ? "" : args.fileSrc;
				$scope.pdfSrc = $sce.trustAsResourceUrl(args.fileSrc);
				//isPDF: model.isSpecialPDFBool,
				//	fileSrc
			});



		}
	}]);



}(angular.module("vrumProject.MobilePreview")));