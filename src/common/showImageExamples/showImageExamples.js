(function(module) {

	module.controller('ShowImageExampleController', ['$scope', 'Endpoint', 'ShowImageExamples', ShowImageExampleController]);

	module.factory('ShowImageExamples', ShowImageExamplesFactory);
	var type = '';

	// Clase para crear un nuevo dialogo
	function ShowImageExamplesDialog($modal) {
		var dialog;

		// Metodo para mostrar el dialogo
		function show(typeE) {
			type = typeE;
			dialog = $modal.open({
				templateUrl: 'showImageExamples/showImageExamples.tpl.html',
				controller: 'ShowImageExampleController as model',
				size: 'lg',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	// El factory retorno un objeto de tipo UpdateVendorInfoDialog
	function ShowImageExamplesFactory($modal) {
		return new ShowImageExamplesDialog($modal);
	}


	function ShowImageExampleController($scope, Endpoint, ShowImageExamples) {
		var model = this;
		model.dismiss = dismiss;

		function dismiss() {
			ShowImageExamples.dismiss();
		}

		function init() {
			var mhpImagesArray = ['assets/mhp1.png', 'assets/mhp2.png', 'assets/mhp3.png', 'assets/mhp4.png', 'assets/mhp5.png',
				'assets/mhp6.png', 'assets/mhp7.png', 'assets/mhp8.png', 'assets/mhp9.png', 'assets/mhp10.png', 'assets/mhp11.png'
			];
			var specialsImagesArray = ['assets/special1.png', 'assets/special2.png', 'assets/special3.png', 'assets/special4.png',
				'assets/special5.png', 'assets/special6.png', 'assets/special7.png', 'assets/special8.png'
			];
			console.log('EL TYPE',type);
			$scope.myInterval = 3000;
			model.myImageArray = (type === 'mhp') ? mhpImagesArray : specialsImagesArray;

			model.title = (type === 'mhp') ? 'Mobile Home Page Sample ' : "Special's Sample";
		}
		init();

	}

}(angular.module("vrumProject.EditMHP")));