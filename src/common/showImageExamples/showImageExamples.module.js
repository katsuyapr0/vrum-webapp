(function(module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('showImageExamples', {
            url: '/showimageexamples',
            views: {
                "main": {
                    controller: 'ShowImageExamplesController as model',
                    templateUrl: 'showImageExamples/showImageExamples.tpl.html'
                }
            },
            data:{ pageTitle: 'ShowImageExamples' }
        });
    });

}(angular.module("vrumProject.showImageExamples", [
    'ui.router'
])));
