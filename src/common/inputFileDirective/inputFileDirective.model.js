(function(module) {
	var _self = this;

	module.service('InputFileDirectiveModelService', ['inputFileDirectiveWebServices', '$state', 'Endpoint', 'VendorInfo', 'AlertsFactory', function(inputFileDirectiveWebServices, $state, Endpoint, VendorInfo, AlertsFactory) {
		var _self = this;
		_self.addTempPdf = addTempPdf;
		function addTempPdf(myFunction, file) {
			inputFileDirectiveWebServices.addTempPdf(file, function(data) {
				console.log(" SE PUDO ENVIAR PDF", data);
				myFunction(data.response.url);
			}, function(data) {
				console.log("NO SE PUDO ENVIAR PDF", data);
			});
		}
	}]);

})(angular.module("vrumProject.signup"));