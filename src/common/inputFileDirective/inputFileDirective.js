(function(module) {
	module.directive('inputMobile', function() {
		return {
			restrict: 'EA',
			controller: 'InputFileDirectiveController as model',
			templateUrl: 'inputFileDirective/inputFileDirective.tpl.html',

			scope: {
				url: '@',
				type: '@',
				callBackMethod: '&getDisplayName'
			},
			link: function(scope) {
				/* send an object to the function */
				console.log(scope.callBackMethod({
					columnName: "hurray"
				}));
			}
		};
	});

	module.controller('InputFileDirectiveController', ['$scope', '$sce', 'InputFileDirectiveModelService','AlertsFactory', function($scope, $sce, InputFileDirectiveModelService, AlertsFactory) {
		var model = this;
		model.fileChange = $scope.fileChange;
		//DATA to send
		var data = {
			file: '',
			imageSrc: '',
			isPDF: '',
			urlPDF:'',
		};
		$scope.fileChange = function(mString) {
			model.isPDFBool = false;
			setTimeout(function() {

				var myString = (mString !== undefined) ? $scope.url : model.myFile.name;
				//console.log('CAMBIO EL FILE', myString);
				//console.log("CAMBIO EL FILE",model.myFile);
				if (model.myFile.size >= 1500000) {

					model.myFile = "assets/no_file.png";
					$scope.imageSrc = "assets/no_file.png";
					$scope.pdfSrc = "assets/no_file.png";
					$scope.$apply();
					AlertsFactory.show('The maximum file size allowed is 1.5 MB.');
					return;
				}
				//console.log("SIGUIO CON EL CODIGO");

				if (~myString.indexOf('.pdf')) {
					model.isPDFBool = true;
					InputFileDirectiveModelService.addTempPdf(function(url) {
						$scope.pdfSrc = $sce.trustAsResourceUrl(url);
						console.log("myPDFURL: ", url);
						data.imageSrc = url;
						data.file = model.myFile;
						data.isPDF = model.isPDFBool;
						data.urlPDF=url;
						$scope.$emit('getFileFromDirective', {
							type: $scope.type,
							'data': data,
						});
					}, model.myFile);
				} else {

					model.isPDFBool = false;
					data.imageSrc = $scope.imageSrc;
					data.file = model.myFile;
					data.isPDF = model.isPDFBool;
					//$rootScope.$emit('getFileFromDirective', $scope.type, data);
					$scope.$emit('getFileFromDirective', {
						type: $scope.type,
						'data': data,
					});
				}

			}, 300);
		};



		init();

		function init() {

			model.isPDFBool = false;
			$scope.imageSrc = $scope.url;
			$scope.pdfSrc = $sce.trustAsResourceUrl("assets/loading.jpg");
			console.log('TYPE INPUT DIRECTIVE', $scope.type);
			if ($scope.type === 'edit') {
				if (~$scope.imageSrc.indexOf('https://') && ~$scope.imageSrc.indexOf('.pdf')) {
					model.isPDFBool = true;
					$scope.pdfSrc = $sce.trustAsResourceUrl($scope.url);
				}
				$scope.fileChange($scope.url);
			}

			var myBroadcast = $scope.type === "mhp" ? "skipMhp" : "skipSpecial";
			
			$scope.$on(myBroadcast, function(event, args) {
				model.isPDFBool = args.isPDF;
				$scope.imageSrc = args.fileSrc;
			});
		}
	}]);



}(angular.module("vrumProject.inputFileDirective")));