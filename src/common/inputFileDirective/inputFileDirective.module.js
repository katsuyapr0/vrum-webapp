(function(module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('inputFileDirective', {
            url: '/inputfiledirective',
            views: {
                "main": {
                    controller: 'InputFileDirectiveController as model',
                    templateUrl: 'inputFileDirective/inputFileDirective.tpl.html'
                }
            },
            data:{ pageTitle: 'InputFileDirective' }
        });
    });

}(angular.module("vrumProject.inputFileDirective", [
    'ui.router'
])));
