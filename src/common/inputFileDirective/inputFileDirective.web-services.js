(function(module) {

	module.service('inputFileDirectiveWebServices', ['$http', 'Endpoint', 'fileUpload', 'VendorInfo', function($http, Endpoint, fileUpload, VendorInfo) {
		var _self = this;

		_self.addTempPdf=addTempPdf;

		function addTempPdf( file, success, error) {
			var url = Endpoint.getVendor() + 'AddTempFile/' ;
			fileUpload.addTempPdf(file, url,success, error);
		}
	}]);

})(angular.module("vrumProject.signup"));