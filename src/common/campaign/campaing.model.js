(function(module) {

  function CampaingModelService($http, CampaingWebServices, Endpoint, $state, CampaignDialog, updateDashBoardService,AlertsFactory) {
    var _self = this;

    // Funcion expuesta
    _self.createCampaing = createCampaing;
    _self.updateCampaign = updateCampaign;
    _self.deleteCampaign = deleteCampaign;
    //_self.addImageCampaing = addImageCampaing;

    function addImageCampaing(campaing_id, file,dashboardScope) {
      CampaingWebServices.addImageCampaing(campaing_id, file, function(data) {
        console.log(" SE PUDO ENVIAR", data);

          updateDashBoardService.updateDashboard(dashboardScope);
      }, function(data) {
        console.log("NO SE PUDO ENVIAR", data);
      });
    }


    function createCampaing(model,name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type, file, dashboardScope) {

      function success(data) {
        if (data.status) {
          addImageCampaing(data.response._id, file,dashboardScope);

          CampaignDialog.dismiss();
          //alert('The special was created');
        } else {
          AlertsFactory.show('The special could not be created');

        }
        model.isProcessing=false;
        //console.log("The special was created", data);

      }

      function error(data) {
        Endpoint.notConnectionAlert();
        model.isProcessing=false;
      }
      model.isProcessing=true;
      CampaingWebServices.createCampaing(name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type).success(success).error(error);
    }

    function updateCampaign(model,id, name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type, file,dashboardScope) {

      function success(data) {
        if (data.status) {
          addImageCampaing(data.response._id, file,dashboardScope);
          updateDashBoardService.updateDashboard(dashboardScope);
          CampaignDialog.dismiss();
          //alert('The special was created');
        } else {
           AlertsFactory.show('The special could not be Updated');

        }
        //console.log("The special was created", data);
      }

      function error(data) {
        Endpoint.notConnectionAlert();
      }
      model.isProcessing=true;
      CampaingWebServices.updateCampaign(id, name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type).success(success).error(error);
    }

    function deleteCampaign(campaign_id,dashboardScope) {

      function success(data) {
        if (data.status) {
          updateDashBoardService.updateDashboard(dashboardScope);
          CampaignDialog.dismiss();
        } else {
          AlertsFactory.show('The special could not be deleted');
        }
        console.log("Campaign Delete", data);
      }

      function error(data) {
        Endpoint.notConnectionAlert();
      }
      CampaingWebServices.deleteCampaign(campaign_id).success(success).error(error);
    }



  }

  module.service('CampaingModelService', [
    '$http',
    'CampaingWebServices',
    'Endpoint',
    '$state',
    'CampaignDialog',
    'updateDashBoardService',
    CampaingModelService
  ]);

}(angular.module("vrumProject.dashboard")));