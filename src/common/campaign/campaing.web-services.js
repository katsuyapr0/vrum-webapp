(function(module) {

  module.service('CampaingWebServices', ['$http', 'Endpoint','fileUpload', function($http, Endpoint,fileUpload) {
    var _self = this;

    // Metodos expuestos
    _self.createCampaing = createCampaing;
    _self.addImageCampaing=addImageCampaing;
    _self.updateCampaign=updateCampaign;
    _self.deleteCampaign=deleteCampaign;

    // Desarrollo de funciones 

    function createCampaing(name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type) {

      return $http({
        url: Endpoint.getVendor() + 'Campaign/' + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'POST',
        data: {
          name: name,
          content: {
            description: {
              en: descriptionEnglish,
              es: descriptionSpanish,
            },
          },
          date_start: date_start,
          date_end: date_end,
          expires: expires,
          type: type,
        },
      });
    }
    function updateCampaign(id,name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type) {
      //app.put('/'+api_version1+'/Vendor/Campaign/:vendor_id/:campaign_id/', model.updateCampaign);
      return $http({
        url: Endpoint.getVendor() + 'Campaign/' + Endpoint.getSessionId() + '/'+id+'/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'PUT',
        data: {
          name: name,
          content: {
            description: {
              en: descriptionEnglish,
              es: descriptionSpanish,
            },
          },
          date_start: date_start,
          date_end: date_end,
          expires: expires,
          type: type,
        },
      });
    }
    function addImageCampaing(campaing_id,file, success, error) {
      var url = Endpoint.getVendor() + 'AddFile/' + localStorage.id + '/';
      var type = 'campaing';

      fileUpload.uploadFileToUrlCampaing(campaing_id,file, url, type, success, error);
    }

    //app.delete('/'+api_version1+'/Vendor/Campaign/:vendor_id/:campaign_id', model.deleteCampaign);

    function deleteCampaign(campaign_id){
      return $http({
        url: Endpoint.getVendor()  + 'Campaign/' + Endpoint.getSessionId() +'/'+ campaign_id,
        headers: Endpoint.getGlobalHeaders(),
        method: 'DELETE',    
      });
    }

  }]);

})(angular.module("vrumProject.campaign"));