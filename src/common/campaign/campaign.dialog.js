(function(module) {

	// COntrolador del dialogo de campaña
	module.controller('CampaignDialogController', ['$scope', '$filter', 'CampaingModelService', 'Endpoint', 'myCampaignValue', 'ShowImageExamples', 'CampaignDialog', 'AlertsFactory', CampaignDialogController]);
	// Factory del campaign Dialog
	module.factory('CampaignDialog', CampaignDialogFactory);

	var dashboardScope = '';

	// Clase para crear un nuevo dialogo
	function AddCampaignDialog($modal) {
		var dialog;

		// Metodo para mostrar el dialogo
		function show(varScope) {
			dashboardScope = varScope;
			dialog = $modal.open({
				templateUrl: 'campaign/campaign.dialog.tpl.html',
				controller: 'CampaignDialogController as model',
				size: 'lg',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	// El factory retorno un objeto de tipo AddCampaignDialog
	function CampaignDialogFactory($modal) {
		return new AddCampaignDialog($modal);
	}
	var completeDate = "";

	function CampaignDialogController($scope, $filter, CampaingModelService, Endpoint, myCampaignValue, ShowImageExamples, CampaignDialog, AlertsFactory) {
		var model = this;
		var myCampaign = '';
		var idLoadedCampaign = '';
		$scope.createBool = true;
		model.showImageExamples = showImageExamples;
		model.dismissDialog = dismissDialog;
		model.isProcessing = false;

		//init();

		// FUncion para obtener si es meridiano
		$scope.toggleMode = function() {
			$scope.ismeridian = !$scope.ismeridian;
		};

		// Funcion para actualizar el tiempo
		$scope.update = function() {
			/*
			var d = new Date();
			d.setHours(14);
			d.setMinutes(0);
			$scope.mytime = d;*/
		};
		completeDateStart = "";
		var completeDateStartArraySplit = '';

		$scope.dtStart = new Date();

		// Funcion cuando cambia el tiempo
		$scope.changedTimeStart = function() {
			var currentDate = $scope.mytimeStart.toString().split(" ");
			completeDateStartArraySplit[4] = currentDate[4];
			completeDateStart = completeDateStartArraySplit.toString().replace(/,/g, " ");
			//console.log('After TIME START: ', completeDateStart);
		};



		$scope.changedDateStart = function() {
			completeDateStartArraySplit = $scope.mytimeStart.toString().split(" ");
			var currentDate = $scope.dtStart.toString().split(" ");
			for (var i = 0; i <= 3; i++) {
				completeDateStartArraySplit[i] = currentDate[i];
			}
			completeDateStart = completeDateStartArraySplit.toString().replace(/,/g, " ");
			//console.log('AfterDATE START: ', completeDateStart);
		};
		// Funcion para abrir el datetime-picker
		$scope.openStart = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.openedStart = true;
		};

		///////////////////
		//FINISHFinish
		/////////////
		completeDateFinish = "";
		var completeDateFinishArraySplit = '';

		$scope.dtFinish = new Date();

		// Funcion cuando cambia el tiempo
		$scope.changedTimeFinish = function() {

			var currentDate = $scope.mytimeFinish.toString().split(" ");
			completeDateFinishArraySplit[4] = currentDate[4];
			completeDateFinish = completeDateFinishArraySplit.toString().replace(/,/g, " ");
			//console.log('After TIME FINISH: ', completeDateFinish);
		};

		$scope.changedDateFinish = function() {
			completeDateFinishArraySplit = $scope.mytimeFinish.toString().split(" ");
			var currentDate = $scope.dtFinish.toString().split(" ");
			for (var i = 0; i <= 3; i++) {
				completeDateFinishArraySplit[i] = currentDate[i];
			}
			completeDateFinish = completeDateFinishArraySplit.toString().replace(/,/g, " ");
			//console.log('AfterDATE FINISH: ', completeDateFinish);
		};

		// Funcion para abrir el datetime-picker
		$scope.openFinish = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.openedFinish = true;
		};
		// funcion para limpiar el tiempo
		$scope.clear = function() {
			$scope.mytime = null;
		};
		// funcion para desactivar el tiempo
		$scope.disabled = function(date, mode) {
			//return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
		};

		$scope.createCampaing = function() {
			$scope.expires = Boolean($scope.expires);
			//console.log("EXPIRES: ", $scope.expires);
			//NO ENCIAR  DESCRIPTION EN ESPAÑOL
			//function createCampaing(name,descriptionEnglish,descriptionSpanish,date_start,date_end,expires,type)
			var name = model.name !== '' ? model.name : '';
			var descriptionEnglish = model.descriptionEnglish !== '' ? model.descriptionEnglish : '';
			var descriptionSpanish = model.descriptionSpanish !== '' ? model.descriptionSpanish : '';
			//var date_start = $scope.expires ? completeDateStart : null;
			//var date_end = $scope.expires ? completeDateFinish : null;
			var date_start = completeDateStart;
			var date_end = completeDateFinish;
			var expires = $scope.expires;
			var type = model.type;

			model.datesCorrectBool = (new Date(date_start).getTime() < new Date(date_end).getTime()) ? true : false;
			model.datesCorrectBool = model.type === "info" ? true : model.datesCorrectBool;

			if ($scope.campaingInfoForm.$valid) {
				console.log('Datos completos');
				console.log('Expires: ' + expires + '/' + 'date_start: ' + date_start);
				if (model.myFile !== '' && model.myFile !== 'assets/no_file.png') {
					if (model.datesCorrectBool) {
						CampaingModelService.createCampaing(model, name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type, model.myFile, dashboardScope);
					}
				} else {
					AlertsFactory.show("Please select a file");
				}
			} else {
				console.log("completa todos los campos save");
				AlertsFactory.show("Please complete your data.");
			}
		};

		$scope.updateCampaign = function() {
			$scope.expires = Boolean($scope.expires);


			var name = model.name !== '' ? model.name : '';
			var descriptionEnglish = model.descriptionEnglish !== '' ? model.descriptionEnglish : '';
			var descriptionSpanish = model.descriptionSpanish !== '' ? model.descriptionSpanish : '';
			var date_start = $scope.expires ? completeDateStart : null;
			var date_end = $scope.expires ? completeDateFinish : null;
			var expires = $scope.expires;
			var type = model.type;

			if ($scope.campaingInfoForm.$valid) {
				CampaingModelService.updateCampaign(model, idLoadedCampaign, name, descriptionEnglish, descriptionSpanish, date_start, date_end, expires, type, model.myFile, dashboardScope);

			} else {
				AlertsFactory.show("Please complete your data.");
			}
		};

		$scope.$on('getFileFromDirective', function(event, args) {
			//console.log('TYPE DESDE LA DIRECTIVE', args.type);
			//console.log('OBJECT DESDE LA DIRECTIVE', args.data);

			model.myFile = args.data.file;
			$scope.imageSrc = args.data.imageSrc;
			//console.log('DATOSDESDE LA DIRECTIVE UPDATE ', model.myFile);

		});

		$scope.deleteCampaign = function() {
			//console.log("Entro a delete ", myCampaign._id);

			CampaingModelService.deleteCampaign(myCampaign._id, dashboardScope);
		};

		$scope.loadCampaignData = function() {
			model.canEditBool = true;
			if (Endpoint.getMyCampaign() !== '') {
				$scope.createBool = false;
				model.titleModal = 'Update Special';

				myCampaign = JSON.parse(Endpoint.getMyCampaign());
				Endpoint.clearMyCampaign();
				idLoadedCampaign = myCampaign._id;
				model._dateStart = '';
				model._dateEnd = '';
				var _dateArrayStart = '';
				var _dateArrayEnd = '';

				//Boolean(myCampaign.expires);
				if (Boolean(myCampaign.expires)) {
					model._dateStart = $filter('date')(new Date(myCampaign.date_start), 'dd/MMM/yyyy ');
					model._dateEnd = $filter('date')(new Date(myCampaign.date_end), 'dd/MMM/yyyy ');
					console.log("INPUT DATE: ", model._dateStart);
					_dateArrayStart = model._dateStart.toString().split(" ");
					_dateArrayEnd = model._dateEnd.toString().split(" ");

				}
				//console.log("DATOS DE ENTRADA CAMPAINGSDIALOG", myCampaign);
				model.name = myCampaign.name;
				model.descriptionEnglish = 'descriptionEnglish';
				model.redemptions = myCampaign.redeemed_by.length;
				model.views = myCampaign.views ? myCampaign.views : 0;
				//model.descriptionSpanish = 'descriptionSpanish';
				model.type = myCampaign.type;
				$scope.dtStart = _dateArrayStart[0];
				$scope.mytimeStart = myCampaign.date_start;
				$scope.dtFinish = _dateArrayEnd[0];
				$scope.mytimeFinish = myCampaign.date_end;
				$scope.expires = Boolean(myCampaign.expires);
				$scope.imageSrc = myCampaign.file.url ? myCampaign.file.url : 'assets/no_file.png';

				var date_1 = new Date(myCampaign.date_start);
				var nowDate = new Date();

				var remainingTime = date_1.getTime() - nowDate.getTime();
				if (remainingTime <= 0) {
					model.canEditBool = false;
				}
				model.canEditBool = false; // SI SE DESEA VALORAR SI SE PEUDE MODIFICAR O NO QUITAR ESTA LINEA
				//console.log('CAN YOU EDIT: ', remainingTime, ' / ', model.canEditBool);
			}
		};

		function showImageExamples(type) {
			ShowImageExamples.show(type);
		}

		function dismissDialog() {
			CampaignDialog.dismiss();
		}
		$scope.$watch("model.myFile", function() {
			//console.log("CAMBIO DE MY FILE", model.myFile);
		});


		function init() {
			model.type = null;
			model.name = null;
			model.datesCorrectBool = true;
			model.myFile = '';
			$scope.formats = ['dd/MMM/yyyy ', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];

			$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};

			$scope.hstep = 1;
			$scope.mstep = 1;
			$scope.mytimeStart = new Date();
			$scope.mytimeFinish = new Date();

			$scope.options = {
				hstep: [1, 2, 3],
				mstep: [1, 5, 10, 15, 25, 30]
			};
			model.titleModal = 'Create a New Special';
			$scope.imageSrc = 'assets/no_file_special.png';
			$scope.loadCampaignData();
			$scope.ismeridian = true;
			$scope.changedDateFinish();
			$scope.changedDateStart();

			$scope.expires = true;
			model.myFile = '';



		}
		init();
	}

}(angular.module("vrumProject.campaign")));