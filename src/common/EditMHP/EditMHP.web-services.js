(function(module) {

  module.service('EditMHPWebServices', ['$http', 'Endpoint','fileUpload', function($http, Endpoint,fileUpload) {
    var _self = this;
    _self.addImageVendor=addImageVendor;

    function addImageVendor(file, success, error) {
      var url = Endpoint.getVendor() + 'AddFile/' + localStorage.id + '/',
        type = 'mhp';

      fileUpload.uploadFileToUrlVendor(file, url, type, success, error);
    }

  }]);

}(angular.module("vrumProject.EditMHP")));