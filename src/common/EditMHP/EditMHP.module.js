(function(module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('EditMHP', {
            url: '/editmhp',
            views: {
                "main": {
                    controller: 'EditMHPController as model',
                    templateUrl: 'EditMHP/EditMHP.tpl.html'
                }
            },
            data:{ pageTitle: 'EditMHP' }
        });
    });

}(angular.module("vrumProject.EditMHP", [
    'ui.router'
])));
