(function(module) {

  function EditMHPModelService($http, EditMHPWebServices, Endpoint, $state,EditMHP,updateDashBoardService,AlertsFactory) {
    var _self = this;
    _self.addImageVendor = addImageVendor;

    function addImageVendor(file,dashboardScope) {
      EditMHPWebServices.addImageVendor(file, function(data) {
        if (data.status) {
          //alert('MHP CHANGED');
          updateDashBoardService.updateDashboard(dashboardScope);
          EditMHP.dismiss();
          console.log("MHP CHANGED'", data);
        } else {
          AlertsFactory.show("The special could not be updated.");
          console.log("NO SE PUDO ENVIAR", data);
        }
      }, function(data) {
        console.log("ERROR", data);
        Endpoint.notConnectionAlert();
      });
    }


  }

  module.service('EditMHPModelService', [
    '$http',
    'EditMHPWebServices',
    'Endpoint',
    '$state',
    'EditMHP',
    'updateDashBoardService',
    EditMHPModelService
  ]);

}(angular.module("vrumProject.EditMHP")));