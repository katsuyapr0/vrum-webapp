(function(module) {

	module.controller('EditMHPController', ['$scope', 'Endpoint', 'EditMHPModelService', 'ShowImageExamples', 'EditMHP', EditMHPController]);

	module.factory('EditMHP', EditMHPFactory);
	var dashboardScope = '';

	// Clase para crear un nuevo dialogo
	function EditMHPDialog($modal) {
		var dialog;

		// Metodo para mostrar el dialogo
		function show(varScope) {
			dashboardScope = varScope;
			dialog = $modal.open({
				templateUrl: 'EditMHP/EditMHP.tpl.html',
				controller: 'EditMHPController as model',
				size: 'lg',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	// El factory retorno un objeto de tipo UpdateVendorInfoDialog
	function EditMHPFactory($modal) {
		return new EditMHPDialog($modal);
	}



	function EditMHPController($scope, Endpoint, EditMHPModelService, ShowImageExamples, EditMHP) {
		var model = this;
		model.editMHP = editMHP;
		model.myFile = '';
		model.showImageExamples = showImageExamples;
		model.dismissDialog = dismissDialog;		

		$scope.$on('getFileFromDirective', function(event, args) {
			console.log('TYPE DESDE LA DIRECTIVE', args.type);
			console.log('OBJECT DESDE LA DIRECTIVE', args.data);

			model.myFile = args.data.file;
			$scope.imageSrc = args.data.imageSrc;
			console.log('DATOSDESDE LA DIRECTIVE UPDATE ', model.myFile);

		});

		function showImageExamples(type) {
			ShowImageExamples.show(type);
		}

		function editMHP() {
			EditMHPModelService.addImageVendor(model.myFile, dashboardScope);
		}

		function dismissDialog() {
			EditMHP.dismiss();
		}

		function init() {
			model.userInfo = JSON.parse(Endpoint.getUserInfoSingleton());
			console.log(model.userInfo);
			if (model.userInfo.mobile_homepage) {
				$scope.imageSrc = model.userInfo.mobile_homepage.page_file.url ? model.userInfo.mobile_homepage.page_file.url : 'assets/no_file_mhp.png';
			} else {
				$scope.imageSrc = 'assets/no_file_mhp.png';
			}


		}
		init();

	}

}(angular.module("vrumProject.EditMHP")));