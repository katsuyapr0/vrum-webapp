(function(module) {

	module.service('ChangePasswordWebServices', ['$http', 'Endpoint', function($http, Endpoint) {
		var _self = this;

		_self.ChangePassword = ChangePassword;


		//Desarrollo de funciones
		// Funcion para llamar el servicio de autenticación
		function ChangePassword(password,newPassword) {
			return $http({
				url: Endpoint.getVendor() + 'ChangePassword/' + localStorage.token + '/',
				data: {
					password: btoa(password),
					new_password : btoa(newPassword)
				},
				headers: Endpoint.getGlobalHeaders(),
				method: 'PUT',
			});
		}

	}]);

})(angular.module("vrumProject.ChangePassword"));