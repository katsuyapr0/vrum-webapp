(function(module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('ChangePassword', {
            url: '/changepassword',
            views: {
                "main": {
                    controller: 'ChangePasswordController as model',
                    templateUrl: 'ChangePassword/ChangePassword.tpl.html'
                }
            },
            data:{ pageTitle: 'ChangePassword' }
        });
    });

}(angular.module("vrumProject.ChangePassword", [
    'ui.router'
])));
