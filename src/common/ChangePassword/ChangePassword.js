(function(module) {

	module.controller('ChangePasswordController', ['$scope', 'Endpoint', 'AlertsFactory', 'ChangePasswordModelService', 'ChangePassword', ChangePasswordController]);

	module.factory('ChangePassword', ChangePasswordFactory);

	// Clase para crear un nuevo dialogo
	function ChangePasswordDialog($modal) {
		var dialog;

		// Metodo para mostrar el dialogo
		function show() {
			dialog = $modal.open({
				templateUrl: 'ChangePassword/ChangePassword.tpl.html',
				controller: 'ChangePasswordController as model',
				size: 's',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	// El factory retorno un objeto de tipo UpdateVendorInfoDialog
	function ChangePasswordFactory($modal) {
		return new ChangePasswordDialog($modal);
	}


	function ChangePasswordController($scope, Endpoint, AlertsFactory, ChangePasswordModelService, ChangePassword) {
		var model = this;
		model.changePassword = changePassword;
		model.dismissDialog = dismissDialog;

		function changePassword() {
			if (!model.newPassword || !model.repeatNewPassword || !model.oldPassword){
				AlertsFactory.show("Please complete your data.");
				return;
			}

			if (model.newPassword === model.repeatNewPassword) {
				ChangePasswordModelService.ChangePassword(model.oldPassword,model.repeatNewPassword,model);
			} else {
				AlertsFactory.show("The passwords doesn't match.");
			}
		}

		function dismissDialog() {
			ChangePassword.dismiss();
		}


		function init() {
			model.userInfo = JSON.parse(Endpoint.getUserInfoSingleton());
			model.oldPassword = '';
			model.newPassword = '';
			model.repeatNewPassword = '';
		}
		init();

	}

}(angular.module("vrumProject.ChangePassword")));