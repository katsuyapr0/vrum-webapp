(function(module) {

  function ChangePasswordModelService($http, ChangePasswordWebServices, Endpoint, $state, AlertsFactory) {
    var _self = this;
    _self.ChangePassword = ChangePassword;

    function ChangePassword(password,newPassword,model) {
      function success(data) {
        if (data.status){
          //window.alert('The password was updated successfully');
          AlertsFactory.show('The password was updated successfully');
          model.dismissDialog();
        }else{
          AlertsFactory.show('The password could not be updated');
        }
      }
      function error(data) {
        Endpoint.notConnectionAlert();
      }

      ChangePasswordWebServices.ChangePassword(password,newPassword).success(success).error(error);
    }
  }

  module.service('ChangePasswordModelService', [
    '$http',
    'ChangePasswordWebServices',
    'Endpoint',
    '$state',
    'AlertsFactory',
    ChangePasswordModelService
  ]);

}(angular.module("vrumProject.ChangePassword")));