(function(app) {
	app.directive("ngFileSelect", function(fileReader) {
		return {
			scope: {
				imageSrc: '=',
			},
			link: function($scope, el) {
				function getFile() {
					fileReader.readAsDataUrl($scope.file, $scope)
						.then(function(result) {
							$scope.imageSrc = result;
						});
				}

				el.bind("change", function(e) {
					$scope.file = (e.srcElement || e.target).files[0];
					getFile();

					if (el.files && el.files[0]) {
						var reader = new FileReader();

						reader.onload = function(e) {
							$scope.imageSrc = e.target.result;
							console.log('src', $scope.imageSrc);
						};
					}

				});
			}
		};
	});
})(angular.module("common.ngFileSelect", []));