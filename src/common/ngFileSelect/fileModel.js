(function(app) {
	app.directive('fileModel', ['$parse', function($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	}]);


	app.service('fileUpload', ['$http', function($http) {

		this.uploadFileToUrlVendor = function(file, uploadUrl, type, success, error) {
			//ENVIAR EN EL BODY name,slogan,
			var fd = new FormData();
			fd.append('file', file);
			fd.append('type', 'mhp');
			$http.put(uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {

						//ARREGLAR EL HEADER
						'Content-Type': undefined,
						type: 'admin',
					}
				})
				.success(success)
				.error(error);
		};
		this.uploadFileToUrlCampaing = function(campaign_id, file, uploadUrl, type, success, error) {
			var fd = new FormData();

			fd.append('file', file);
			fd.append('type', 'campaign');
			fd.append('campaign_id', campaign_id);

			$http.put(uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined,
						type: 'admin',
					}
				})
				.success(success)
				.error(error);
		};

		this.addTempPdf = function(file, uploadUrl, success, error) {
			var fd = new FormData();
			fd.append('file', file);
			$http.put(uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined,
						type: 'admin',
					}
				})
				.success(success)
				.error(error);
		};
	}]);
})(angular.module("common.fileModel", []));