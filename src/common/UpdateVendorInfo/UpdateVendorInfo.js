(function(module) {


	// COntrolador del dialogo de campaña
	module.controller('UpdateVendorInfoController', ['$scope', 'Endpoint', 'UpdateVendorInfoModelService', 'UpdateVendorInfo', '$filter', 'SignupModelService', UpdateVendorInfoController]);
	// Factory del campaign Dialog
	module.factory('UpdateVendorInfo', UpdateVendorInfoFactory);
	var dashboardScope = '';

	// Clase para crear un nuevo dialogo
	function UpdateVendorInfoDialog($modal) {
		var dialog;

		// Metodo para mostrar el dialogo
		function show(varScope) {
			dashboardScope = varScope;
			dialog = $modal.open({
				templateUrl: 'UpdateVendorInfo/UpdateVendorInfo.tpl.html',
				controller: 'UpdateVendorInfoController as model',
				size: 'lg',
			});
		}

		// Metodo para ocultar el dialogo
		function dismiss() {
			dialog.dismiss('cancel');
		}

		// retorna los valores/funciones expuestos
		return {
			show: show,
			dismiss: dismiss,
			instance: dialog,
		};
	}

	// El factory retorno un objeto de tipo UpdateVendorInfoDialog
	function UpdateVendorInfoFactory($modal) {
		return new UpdateVendorInfoDialog($modal);
	}



	function UpdateVendorInfoController($scope, Endpoint, UpdateVendorInfoModelService, UpdateVendorInfo, $filter, SignupModelService) {
		var model = this;
		model.userInfo = {};
		model.updateVendorInfoFunction = updateVendorInfoFunction;
		model.getZipCode = getZipCode;
		model.dismissDialog = dismissDialog;
		model.formatTel = formatTel;
		model.categoryCurrent = "";
		model.validBusinessPhone = true;
		model.validBusinessPhone2 = true;
		model.validPhone = true;


		function objectToSend() {
			return {
				name: model.userInfo.name_original,
				main_address: {
					phone: model.userInfo.main_address.phone,
					phone2: model.userInfo.main_address.phone2,
					city: model.userInfo.main_address.city,
					state: model.userInfo.main_address.state,
					zip: model.userInfo.main_address.zip,
					street1: model.userInfo.main_address.street1,
					street2: model.userInfo.main_address.street2,
					category: model.category,
				},
				contact_info: {
					name: model.userInfo.contact_info.name,
					lastname: model.userInfo.contact_info.lastname,
					phone: model.userInfo.contact_info.phone,
					phone2: model.userInfo.contact_info.phone,
					category: model.category,
				},
				category: model.category,
			};
		}

		function updateVendorInfoFunction() {
			if ($scope.bussinesInformationForm.$valid) {

				model.validBusinessPhone = false;
				model.validBusinessPhone2 = false;
				model.validPhone = false;


				if (model.userInfo.main_address.phone.length > 10) {
					model.validBusinessPhone = true;
				} else {
					model.validBusinessPhone = false;
				}

				if (model.userInfo.main_address.phone2 && model.userInfo.main_address.phone2.length > 0) {
					model.validBusinessPhone2 = (model.userInfo.main_address.phone2.length > 10) ? true : false;
				} else {
					model.validBusinessPhone2 = true;
				}

				if (model.userInfo.contact_info.phone.length > 10) {
					model.validPhone = true;
				} else {
					model.validPhone = false;
				}

				if (!model.validBusinessPhone || !model.validBusinessPhone2 || !model.validPhone) {
					return;
				}
				//console.log('Datos completos');
				UpdateVendorInfoModelService.updateVendorInfo(dashboardScope, objectToSend());
			} else {
				//console.log("completa todos los campos");
			}
		}

		function getZipCode() {
			if (model.userInfo.main_address.zip.length === 5) {
				UpdateVendorInfoModelService.getZipCode(model.userInfo.main_address.zip, function(city, state) {
					model.userInfo.main_address.city = city;
					model.userInfo.main_address.state = state;
				});
			}
		}

		function getCategoryList() {
			model.categoryList = {};
			SignupModelService.getCategoryList(function(data) {
				model.categoryList = data;
				//console.log("model.categoryList", model.categoryList);
			});

		}

		function dismissDialog() {
			UpdateVendorInfo.dismiss();
		}

		function formatTel(number, type) {
			model.userInfo.contact_info.phone = (type === "model.userInfo.contact_info.phone") ? $filter('tel')(number) : model.userInfo.contact_info.phone;
			model.userInfo.main_address.phone2 = (type === "model.userInfo.main_address.phone2") ? $filter('tel')(number) : model.userInfo.main_address.phone2;
			model.userInfo.main_address.phone = (type === "model.userInfo.main_address.phone") ? $filter('tel')(number) : model.userInfo.main_address.phone;
		}

		function fixName(text) {
			var temp = text.split("");
			temp[0] = temp[0].toUpperCase();
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] === " " && i < temp.length - 2) {
					temp[i + 1] = temp[i + 1].toUpperCase();
				}
			}
			return temp.join("");
		}


		function init() {
			getCategoryList();
			model.userInfo = JSON.parse(Endpoint.getUserInfoSingleton());
			model.userInfo.name_original=fixName(model.userInfo.name_original);

			//console.log('Update user info Data :', model.userInfo);
			if (model.userInfo.category) {
				model.categoryCurrent = model.userInfo.category.name.en;
			}
		}
		init();

	}
	module.filter('tel', function() {
		return function(tel) {
			if (!tel) {
				return '';
			}

			var value = tel.toString().trim().replace(/^\+/, '');

			if (value.match(/[^0-9]/)) {
				return tel;
			}

			var country, city, number;

			switch (value.length) {
				case 10: // +1PPP####### -> C (PPP) ###-####
					country = 1;
					city = value.slice(0, 3);
					number = value.slice(3);
					break;

				case 11: // +CPPP####### -> CCC (PP) ###-####
					country = value[0];
					city = value.slice(1, 4);
					number = value.slice(4);
					break;

				case 12: // +CCCPP####### -> CCC (PP) ###-####
					country = value.slice(0, 3);
					city = value.slice(3, 5);
					number = value.slice(5);
					break;

				default:
					return tel;
			}

			if (country == 1) {
				country = "";
			}

			number = number.slice(0, 3) + '-' + number.slice(3);

			return (country + " (" + city + ") " + number).trim();
		};
	});

}(angular.module("vrumProject.UpdateVendorInfo")));