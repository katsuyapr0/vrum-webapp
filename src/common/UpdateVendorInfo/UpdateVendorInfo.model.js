(function(module) {

  function UpdateVendorInfoModelService($http, UpdateVendorInfoWebServices, Endpoint, $state, UpdateVendorInfo, updateDashBoardService,AlertsFactory) {
    var _self = this;

    _self.updateVendorInfo = updateVendorInfo;
    _self.getZipCode = getZipCode;
    _self.getCategoryList=getCategoryList;

    function updateVendorInfo(dashboardScope, updateData) {
      function success(data) {
        if (data.status) {
          Endpoint.setUserInfoSingleton(data.response);
          updateDashBoardService.updateDashboard(dashboardScope);
          UpdateVendorInfo.dismiss();
          console.log("El vendor fue actualizado", data);
          //alert('The vendor was update');
        } else {
          AlertsFactory.show('The vendor info could not be updated');
        }
      }

      function error(data) {
        Endpoint.notConnectionAlert();
      }
      UpdateVendorInfoWebServices.updateVendorInfo(updateData).success(success).error(error);
    }


    function getZipCode(zipCode, myFunction) {
      function success(data) {
        console.log("GET ZIP CODE_ :", data);
        if (data.status) {
          myFunction(data.response.city, data.response.state);
        } else {
          myFunction('', '');
        }
      }

      function error(data) {

      }
      UpdateVendorInfoWebServices.getZipCode(zipCode).success(success).error(error);
    }

    function getCategoryList(myFunction) {
      function success(data) {
        console.log("get CATEGORIES", data);
        if (data.status) {
          myFunction(data.response);
        } else {

        }
      }

      function error(data) {
        //Endpoint.notConnectionAlert();
      }
      SignupWebServices.getCategoryList().success(success).error(error);
    }


  }

  module.service('UpdateVendorInfoModelService', [
    '$http',
    'UpdateVendorInfoWebServices',
    'Endpoint',
    '$state',
    'UpdateVendorInfo',
    'updateDashBoardService',
    UpdateVendorInfoModelService
  ]);
}(angular.module("vrumProject.UpdateVendorInfo")));