(function(module) {

    module.config(function($stateProvider) {
        $stateProvider.state('UpdateVendorInfo', {
            url: '/updatevendorinfo',
            views: {
                "main": {
                    controller: 'UpdateVendorInfoController as model',
                    templateUrl: 'UpdateVendorInfo/UpdateVendorInfo.tpl.html'
                }
            },
            data: {
                pageTitle: 'UpdateVendorInfo'
            }
        });
    });

}(angular.module("vrumProject.UpdateVendorInfo", [
    'ui.router'
])));