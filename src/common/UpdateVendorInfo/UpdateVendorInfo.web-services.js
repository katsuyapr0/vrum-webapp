(function(module) {

  module.service('UpdateVendorInfoWebServices', ['$http', 'Endpoint', 'fileUpload', function($http, Endpoint, fileUpload) {
    var _self = this;
    _self.updateVendorInfo = updateVendorInfo;
    _self.getZipCode = getZipCode;

    function updateVendorInfo(updateData) {
      return $http({
        url: Endpoint.getVendor() + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'PUT',
        data: updateData,
      });
    }

    function getZipCode(zipCode) {
      return $http({
        url: Endpoint._getEndPoint() + 'Zipcode/' + zipCode,
        method: 'GET',
      });
    }



  }]);
}(angular.module("vrumProject.UpdateVendorInfo")));