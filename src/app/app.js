(function(app) {

  app.run(['$rootScope', '$state', function($rootScope, $state) {
    $rootScope.$on("$stateChangeStart", function(e, toState, toParams, fromStatate, fromParams) {
      if (toState.name === 'login' || toState.name === 'signup' ||toState.name==="support") {
        return;
      }
      if (!localStorage.getItem("isLogin")) {
        e.preventDefault();
        $state.go("login");
      }

    });
  }]);


  app.run(function() {});

  //Controlador padre de toda la aplicación
  app.controller('AppController', function($scope) {

  });

  // Constante para definir el endpoint de los servicios web
  app.constant('EndpointConstant', {
    dev: 'http://192.241.187.135:2100/v1/',
    //dev: 'http://andres.local:2100/v1/',
    production: 'https://vrum.mobi:2100/v1/',
    isDebug: true,
  });

  app.constant('VendorInfo', {
    vendor: {},
  });

  // valores que se van a usar en toda la aplicación
  app.value('SessionValue', {
    token: '',
    id: '',
    email: '',
    password: '',
  });
  app.value('myCampaignValue', {
    campaign: '',
  });

  app.value('UserInfoSingleton', {
    userInfo: '',
  });

  localStorage.setItem('messageToAlert', 'This is a message');

  // Servicio para obtener el endpoint
  app.service('Endpoint', ['EndpointConstant', 'SessionValue', 'UserInfoSingleton', 'myCampaignValue', 'AlertsFactory', function(EndPointConstant, SessionValue, UserInfoSingleton, myCampaignValue, AlertsFactory) {
    var _self = this,

      // rutas de los servicios(solo esta Vendor, pero si se crean más servicios hay que agregarlo aqui)
      OBJECTS = {
        vendor: 'Vendor',
      };

    // ----------------------------------------------------------------------------------------------------
    // Funciones expuestas en el Endpoint' service
    // ----------------------------------------------------------------------------------------------------
    _self.getGlobalHeaders = getGlobalHeaders;
    _self.getExpiredHeaders = getExpiredHeaders;
    _self.setSessionToken = setSessionToken;
    _self.setSessionId = setSessionId;
    _self._getEndPoint = _getEndPoint;
    _self.getVendor = getVendor;
    _self.setUser = setUser;
    _self.getSessionId = getSessionId;
    _self.getImageHeaders = getImageHeaders;

    _self.setUserInfoSingleton = setUserInfoSingleton;
    _self.getUserInfoSingleton = getUserInfoSingleton;

    _self.setMyCampaign = setMyCampaign;
    _self.getMyCampaign = getMyCampaign;
    _self.clearMyCampaign = clearMyCampaign;

    _self.notConnectionAlert = notConnectionAlert;

    // ----------------------------------------------------------------------------------------------------
    // Desarrollo de las funciones
    // ----------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------------------------------------
    // Funciones privadas
    // ----------------------------------------------------------------------------------------------
    function notConnectionAlert() {
      AlertsFactory.show('Something It\'s wrong. Please check your internet connection');

    }

    function setUserInfoSingleton(userInfoJSONObject) {
      UserInfoSingleton.userInfo = JSON.stringify(userInfoJSONObject);
    }

    function getUserInfoSingleton() {
      return UserInfoSingleton.userInfo;
    }

    function setMyCampaign(campaignJSON) {
      console.log("setMyCampaign ", campaignJSON);
      myCampaignValue.campaign = JSON.stringify(campaignJSON);
    }

    function getMyCampaign() {
      return myCampaignValue.campaign;
    }

    function clearMyCampaign() {
      myCampaignValue.campaign = '';
    }

    // Función para obtener el endpoint, dependiendo de si esta en debug o en production
    function _getEndPoint() {
      return EndPointConstant.isDebug ? EndPointConstant.dev : EndPointConstant.production;
    }

    // ----------------------------------------------------------------------------------------------
    // Funciones publicas
    // ----------------------------------------------------------------------------------------------

    // Función para obtener el endpoint del vendedor
    function getVendor() {
      return _getEndPoint() + OBJECTS.vendor + '/';
    }

    // Función para obtener los headers de seguridad de los servicios web
    function getGlobalHeaders() {
      return {
        type: 'vendor',
        //Authorization: 'Basic ' + btoa(SessionValue.email + ':' + SessionValue.password),
        Authorization: 'Basic ' + btoa(localStorage.email + ':' + atob(localStorage.password)),
        token: localStorage.token,
        //token: SessionValue.token,

        _id: btoa(SessionValue.id),
      };
    }

    function getImageHeaders() {
      return {
        type: 'vendor',
        //Authorization: 'Basic ' + btoa(SessionValue.email + ':' + SessionValue.password),
        Authorization: 'Basic ' + btoa(localStorage.email + ':' + atob(localStorage.password)),
        'Content-Type': 'multipart/form-data',
        token: localStorage.token,
        //token: SessionValue.token,

        _id: btoa(SessionValue.id),
      };
    }

    //'Content-Type' : 'multipart/form-data'

    /**
     * This function is only for testing a user which session expired
     */
    function getExpiredHeaders() {
      return {
        type: 'vendor',
        Authorization: 'Basic ' + btoa(SessionValue.email + ':' + SessionValue.password),
        expired: true, // Only for testing expired session
        token: SessionValue.token,

        _id: btoa(SessionValue.id),
      };
    }

    // Guarda el session token para despues usarlo en los headers
    function setSessionToken(token) {
      localStorage.setItem('token', token);
      SessionValue.token = token;
    }

    // Guarda el id para despues usarlo en los headers
    function getSessionId() {
      SessionValue.id = localStorage.id;
      return SessionValue.id;
    }

    // Guarda el usuario y el password para usarlo dentro de los headers
    function setUser(email, password) {
      localStorage.setItem('email', email);
      localStorage.setItem('password', password);

      SessionValue.email = localStorage.email;
      SessionValue.password = localStorage.password;
    }

    // Guarda el session id para usarlo dentro de los headers
    function setSessionId(id) {
      localStorage.setItem('id', id);
      SessionValue.id = id;
    }
  }]);

}(angular.module("vrumProject", [
  // dependencias de la aplicación
  'sbAdminApp',
  'vrumProject.home',
  'vrumProject.about',
  'templates-app',
  'templates-common',
  'ui.router.state',
  'ui.router',
  'ui.bootstrap',
  'vrumProject.login',
  'vrumProject.signup',
  'vrumProject.dashboard',
  'common.ngFileSelect',
  'common.fileReader',
  'common.fileModel',
  'angularPayments',
  'vrumProject.campaign',
  'vrumProject.CampaignTimeline',
  'vrumProject.UpdateVendorInfo',
  'vrumProject.ChangePassword',
  'vrumProject.EditMHP',
  'vrumProject.alerts',
  'vrumProject.MobilePreview',
  'vrumProject.inputFileDirective',
  'vrumProject.showImageExamples',
  'vrumProject.payments',
  'vrumProject.support'
]).factory('Interceptor', function($q, $location, $window, $rootScope) {
  return {
    // optional method

    request: function(req) {
      if (!$rootScope.callCounter) {
        $rootScope.callCounter = 0;
      }
      $rootScope.callCounter++;
      $rootScope.showSpinner = true;
      $('#mydiv').show();
      return req;
    },

    'response': function(response) {
      // do something on success
      $rootScope.callCounter--;
      if ($rootScope.callCounter === 0) {
        $('#mydiv').hide();
      }

      return response || $q.when(response);
    },


    'responseError': function(rejection) {
      $rootScope.callCounter--;
      if ($rootScope.callCounter === 0) {
        $('#mydiv').hide();
        if (rejection.status === 401) {
          if (app.isLogged()) {
            $rootScope.$broadcast('unauthorized');
          }
        }
      }
      if (rejection.status != 401) {
        $rootScope.$broadcast('failure');
      }
      console.log(rejection);
      return $q.reject(rejection);
    }
  };
}).config(["$stateProvider", "$urlRouterProvider", "$httpProvider", function($stateProvider, $urlRouterProvider, $httpProvider) {
  $urlRouterProvider.otherwise('/login');
  $httpProvider.interceptors.push('Interceptor');

}])));