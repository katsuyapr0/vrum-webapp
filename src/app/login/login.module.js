(function(module) {
  // Declara la ruta de login
  module.config(function ($stateProvider) {
    $stateProvider.state('login', {
      url: '/login',
      views: {
        "main": {
          controller: 'LoginController as model',
          templateUrl: 'login/login.tpl.html'
        }
      },
      data:{ pageTitle: 'Login' }
    });
  });

}(angular.module("vrumProject.login", [
    'ui.router'
])));
