(function(module) {

  function LoginModelService($http, LoginWebServices, Endpoint, $state, AlertsFactory) {
    var _self = this;

    // Metodos expuestos
    _self.getVendor = getVendor;
    _self.onLogin = onLogin;

    // Desarrollo de funciones

    // Funcion para obtener información del vendor
    function getVendor() {
      // Cuando logra obtener la informacion del Vendor
      function success(data) {
        //console.log(data);
      }
      // Cuando no logra obtener la informacion del Vendor
      function error(data) {
        Endpoint.notConnectionAlert();
        //console.log(data);
      }

      // Llama el servicio para obtener la información del vendor
      LoginWebServices.getVendorInfo().success(success).error(error);
    }

    // Funcion para llamar el servicio de autenticación
    function onLogin(email, password,remember) {
      // Cuando la autenticación es correcta
      function success(data) {
        //console.log(data);
        // Verifica el status del servicio
        if (data.status) {
          localStorage.setItem('isLogin',true);
          Endpoint.setSessionToken(data.token.token);
          Endpoint.setSessionId(data.response._id);
          Endpoint.setUser(email.toLowerCase(), btoa(password));
          if(remember===true){
            localStorage.setItem('remember',remember);
          }
          $state.go('dashboard');
        } else {
          AlertsFactory.show(data.error);
        }
      }
      // Cuando la autenticación es incorrecta
      function error(data) {
        //alert('Something It\'s wrong. Please check your internet conection.');
        Endpoint.notConnectionAlert();
      }

      // Llama el servicio para autenticarse
      //console.log("Login para ",email,' pwd ',password);
      LoginWebServices.loginVendor(email.toLowerCase(), password).success(success).error(error);
    }

  }

  module.service('LoginModelService', [
    '$http',
    'LoginWebServices',
    'Endpoint',
    '$state',
    'AlertsFactory',
    LoginModelService
  ]);

}(angular.module("vrumProject.login")));