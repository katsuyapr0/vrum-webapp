(function (module) {

    module.config(function ($stateProvider) {
        $stateProvider.state('support', {
            url: '/support',
            views: {
                "main": {
                    controller: 'SupportController as model',
                    templateUrl: 'support/support.tpl.html'
                }
            },
            data: {
                pageTitle: 'Support'
            }
        });
    });

}(angular.module("vrumProject.support", [
    'ui.router'
])));