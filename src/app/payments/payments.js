(function(module) {

    module.controller('PaymentsController', ['$state', '$window', '$scope', 'PaymentsModelService', 'AlertsFactory', 'Endpoint', function($state, $window, $scope, PaymentsModelService, AlertsFactory, Endpoint) {
        var model = this;



        /*Initialize view*/
        model.view = {
            vendorEmail: "",
            selectedPlan: ""
        };

        /*END Initialize view*/

        /*Initialize variables*/

        model.availablePlans = [];
        model.paymentHistoryItems = [];
        model.vendorInfo = null;

        /*END Initialize variables*/

        /*
         * Function that redirects the user to the dashboard
         *
         * */
        model.goToDashboard = function() {
            $state.go('dashboard');
        };
        model.formatAmount = function(amount) {
            console.log(amount);
            amount = amount + '';
            var valueToReturn = '$ ';
            var amountArr = amount.split("");
            for (var i = 0; i < amountArr.length; i++) {
                if (i < amountArr.length - 2) {
                    valueToReturn = valueToReturn + amountArr[i];
                } else if (i === amountArr.length - 2) {
                    valueToReturn = valueToReturn + '.' + amountArr[i] + amountArr[i + 1];
                    break;
                }
            }
            return valueToReturn;
        };

        /*
         * Function that creates a new subscription on the server
         *
         * */
        model.createSubscription = function(token) {
            PaymentsModelService.createSubscription(model.view.vendorEmail, model.view.selectedPlan.id, token).then(function(res) {
                if (res.status === 200) {
                    if (res.data.error) {
                        AlertsFactory.show(res.data.error);
                    } else {
                        AlertsFactory.show("Payment Accepted.");
                        //$state.reload();
                        init();
                    }

                }
            });
        };

        model.selectPlan = function(plan) {
            if (model.view.selectedPlan) {
                model.view.selectedPlan.isSelected = false;
            }
            model.view.selectedPlan = plan;
            model.view.selectedPlan.isSelected = true;
        };

        model.resolveLabelForEventType = function(eventType) {
            if ("charge.succeeded" == eventType) {
                return "Payment Accepted";
            }
            if ("customer.deleted" == eventType) {
                return "Subscription Deleted";
            }
            if ("invoice.payment_succeeded" == eventType) {
                return "Invoice Payment Succeeded";
            }
            return eventType;
        };

        model.updatePaymentHistory = function() {
            if (model.vendorInfo && model.vendorInfo.stripe && model.vendorInfo.stripe.is_active) {
                PaymentsModelService.getPaymentsHistory(model.vendorInfo._id).then(function(res) {
                    if (res.status === 200) {
                        model.paymentHistoryItems = res.data.response;
                        console.log("History ", model.paymentHistoryItems);
                    }
                });
            }
        };

        init();

        //$scope.$watch('model.vendorInfo',model.updatePaymentHistory());

        function init() {
            $scope.Endpoint = Endpoint;
            model.view.vendorEmail = localStorage.email;

            PaymentsModelService.getPlans().then(function(res) {
                if (res.status === 200) {
                    model.availablePlans = res.data.response;
                    if (model.availablePlans && model.availablePlans.length > 0) {
                        model.selectPlan(model.availablePlans[2]);
                    }
                }
            });

            PaymentsModelService.getVendorInfo().then(function(res) {
                if (res.status === 200) {
                    model.vendorInfo = res.data.response;
                    //console.log("Vendor Info ", model.vendorInfo);
                    model.updatePaymentHistory();
                }
            });

            PaymentsModelService.getPaymentToken().then(function(res) {
                if (res.status === 200) {
                    //console.log("Token ", res.data.response);
                    var handler = $window.StripeCheckout.configure({
                        key: res.data.response,
                        image: '/assets/logo.png',
                        locale: 'auto',
                        token: function(token) {
                            model.createSubscription(token.id);
                            // Use the token to create the charge with a server-side script.
                            // You can access the token ID with `token.id`
                        }
                    });

                    $('#paymentButton').on('click', function(e) {
                        // Open Checkout with further options
                        handler.open({
                            name: 'Vrum',
                            description: model.view.selectedPlan.name,
                            zipCode: true,
                            amount: model.view.selectedPlan.amount
                        });
                        e.preventDefault();
                    });

                    // Close Checkout on page navigation
                    $(window).on('popstate', function() {
                        handler.close();
                    });
                }
            });


        }
    }]);

}(angular.module("vrumProject.payments")));