(function(module) {

    function PaymentsModelService($http, Endpoint, $state, AlertsFactory) {
        var _self = this;

        /*
        * Metodo que consulta los planes disponibles
        *
        * */
        _self.getPlans = function(){
            return $http({
                method: 'GET',
                url:  Endpoint._getEndPoint() + 'stripe/plans/',
                headers: Endpoint.getGlobalHeaders(),
                data: null
            });
        };

        _self.getVendorInfo=function() {
            return $http({
                url: Endpoint.getVendor() + Endpoint.getSessionId() + '/',
                headers: Endpoint.getGlobalHeaders(),
                //headers: Endpoint.getExpiredHeaders(), // Only for testing expired session
                method: 'GET',
            });
        };


        _self.createSubscription=function(email,selectedPlan,token){
            return $http({
                method: 'POST',
                url:  Endpoint._getEndPoint() + 'stripe/subscribe/',
                headers: Endpoint.getGlobalHeaders(),
                data: {
                    email: email,
                    plan: selectedPlan,
                    stripeToken: token
                },
            });
        };

        _self.getPaymentsHistory=function(vendorId){
            return $http({
                method: 'GET',
                url:  Endpoint.getVendor() + 'Transactions/'+vendorId+'/',
                headers: Endpoint.getGlobalHeaders(),
                data: null,
            });
        };

        _self.getPaymentToken=function(vendorId){
            return $http({
                method: 'GET',
                url:  Endpoint._getEndPoint() + 'stripe/key/',
                headers: Endpoint.getGlobalHeaders(),
                data: null,
            });
        };


    }

    module.service('PaymentsModelService', [
        '$http',
        'Endpoint',
        '$state',
        'AlertsFactory',
        PaymentsModelService
    ]);

}(angular.module("vrumProject.payments")));/**
 * Created by julianmontana on 2/17/16.
 */
