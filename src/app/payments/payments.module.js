(function(module) {
    
    module.config(function ($stateProvider) {
        $stateProvider.state('payments', {
            url: '/payments',
            views: {
                "main": {
                    controller: 'PaymentsController as model',
                    templateUrl: 'payments/payments.tpl.html'
                }
            },
            data:{ pageTitle: 'Payments' }
        });
    });
    
}(angular.module('vrumProject.payments', [
    'ui.router'
])));
