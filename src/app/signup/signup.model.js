(function(module) {
	var _self = this;

	module.service('SignupModelService', ['SignupWebServices', '$state', 'Endpoint', 'VendorInfo', 'AlertsFactory', function(SignupWebServices, $state, Endpoint, VendorInfo, AlertsFactory) {
		var _self = this;
		var registerData = '';
		var specialData = '';
		var mhpData = '';

		// Metodos expuestos
		_self.receiveData = receiveData;
		_self.getZipCode = getZipCode;
		_self.addTempPdf = addTempPdf;
		_self.existsEmail=existsEmail;
		_self.getCategoryList=getCategoryList;



		function receiveData(register, mhp, special) {
			registerData = register;
			mhpData = mhp;
			specialData = special;

			console.log('@DATA REGISTER', registerData);
			console.log('@DATA MHP', mhpData);
			console.log('@DATA SPECIAL', specialData);

			onSignup();

		}

		function addImageVendor() {
			if (mhpData !== '') {
				SignupWebServices.sendMph(mhpData.file, function(data) {
					/*
					function success(data) {
						if (data.status) {
							console.log("The Mhp info was updated", data);
						} else {
validate							console.log('The Mhp info was NOT updated', data);
						}
					}

					function error(data) {
						Endpoint.notConnectionAlert();
					}*/
					if (data.status) {
						console.log("MHP CHANGED'", data);


						//SignupWebServices.updateMhpInfo(mhpData.data).success(success).error(error);
					} else {
						console.log("NO SE PUDO ENVIAR", data);
					}
				}, function(data) {
					console.log("ERROR", data);
					Endpoint.notConnectionAlert();
				});
			}

		}

		function addImageCampaing(campaing_id, file) {
			SignupWebServices.addImageCampaing(campaing_id, file, function(data) {
				console.log(" SE PUDO ENVIAR IMAGEN SPECIAL", data);
			}, function(data) {
				Endpoint.notConnectionAlert();
				console.log("NO SE PUDO ENVIAR IMAGEN SPECIAL", data);
			});
		}

		function createCampaing() {
			function success(data) {
				if (data.status) {
					console.log("The special was created", data);
					addImageCampaing(data.response._id, specialData.file);
				} else {
					console.log('The special could not be created');
				}

			}

			function error(data) {
				Endpoint.notConnectionAlert();
			}
			if (specialData.file !== '') {
				SignupWebServices.createCampaing(specialData.data).success(success).error(error);
			}

		}

		// Funcion para actualizar informacion del Vendor, mas especificamente enviar informacion del mph
		function onUpdateInfo(file, info, success, error) {
			// sube el archivo del mph
			SignupWebServices.sendMph(file, function(data) {
				if (data.status) {
					// si se puede subir se actualiza la otra informacion
					SignupWebServices.updateMhpInfo(info).success(success, error);
				} else {
					error();
				}
			}, error);
		}

		function onSignup() {
			var password = registerData.password;
			registerData.password = btoa(registerData.password);

			function success(data) {

				if (data.status) {
					console.log('Se creo el user: ', data);
					Endpoint.setSessionId(data.response._id);
					Endpoint.setUser(registerData.email, registerData.password);
					VendorInfo.vendor = data.response;
					setTimeout(function(){addImageVendor();},1000);
					setTimeout(function(){createCampaing();},2000);
					
					//AlertsFactory.show('Congratulations, you are VRüMing now!');
					AlertsFactory.show('We sent you an email.  Please click on the link to validate your account.');
					
					$state.go('login');

				} else {
					console.log('El user no pudo ser creado: ', data);
				}
			}

			function error(data) {
				Endpoint.notConnectionAlert();
				console.log(data);
			}
			SignupWebServices.createVendor(registerData).success(success).error(error);
		}


		function getZipCode(zipCode, myFunction) {
			function success(data) {
				console.log("GET ZIP CODE_ :", data);
				if (data.status) {
					myFunction(data.response.city, data.response.state,true);
				} else {
					myFunction('', '',false);
				}
			}

			function error(data) {
				Endpoint.notConnectionAlert();
			}
			SignupWebServices.getZipCode(zipCode).success(success).error(error);
		}
		
		function existsEmail(email,myFunction) {
			function success(data) {
				console.log("EXIST EMAIL", data);				
				myFunction(data.status);				
			}

			function error(data) {
				//Endpoint.notConnectionAlert();
			}
			SignupWebServices.existsEmail(email).success(success).error(error);
		}

		function addTempPdf(myFunction, file) {
			SignupWebServices.addTempPdf(file, function(data) {
				//console.log(" SE PUDO ENVIAR PDF", data);
				myFunction(data.response.url);
			}, function(data) {
				//console.log("NO SE PUDO ENVIAR PDF", data);
			});
		}

		function getCategoryList(myFunction) {
			function success(data) {
				//console.log("get CATEGORIES", data);
				if (data.status) {
					myFunction(data.response);
				} else {

				}
			}
			function error(data) {
				Endpoint.notConnectionAlert();
			}
			SignupWebServices.getCategoryList().success(success).error(error);
		}

	}]);

})(angular.module("vrumProject.signup"));