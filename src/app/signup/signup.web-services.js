(function(module) {

	module.service('SignupWebServices', ['$http', 'Endpoint', 'fileUpload', 'VendorInfo', function($http, Endpoint, fileUpload, VendorInfo) {
		var _self = this;

		// Metodos expuestos
		_self.createVendor = createVendor;
		_self.sendMph = sendMph;
		_self.updateMhpInfo = updateMhpInfo;
		_self.getZipCode = getZipCode;
		_self.createCampaing = createCampaing;
		_self.addImageCampaing=addImageCampaing;
		_self.addTempPdf=addTempPdf;
		_self.existsEmail=existsEmail;
		_self.getCategoryList=getCategoryList;


		// Desarrollo de las funciones
		// Llama al servicio para subir una imagen a mph
		function sendMph(file, success, error) {
			var url = Endpoint.getVendor() + 'AddFile/' + Endpoint.getSessionId() + '/',
				type = 'mhp';

			fileUpload.uploadFileToUrlVendor(file, url, type, success, error);
		}

		// Llama al servicio para actualizar la informacion de mph
		/*function updateMhpInfo(mhp) {
			VendorInfo.vendor.mobile_homepage = {
				name: mhp.name || VendorInfo.vendor.mobile_homepage.name,
				description: mhp.description || VendorInfo.vendor.mobile_homepage.description,
			};

			return $http({
				url: Endpoint.getVendor() + VendorInfo.vendor._id + '/',
				data: VendorInfo,
				method: 'PUT'
			});
		}*/

		function updateMhpInfo(mhp) {
			return $http({
				url: Endpoint.getVendor() + Endpoint.getSessionId()+ '/',
				headers: Endpoint.getGlobalHeaders(),
				data: mhp.data,
				method: 'PUT',
			});
		}

		// Llama la funcion para crear un vendor
		function createVendor(vendor) {
			return $http({
				url: Endpoint.getVendor(),
				data: vendor,
				method: 'POST',
			});
		}

		function getZipCode(zipCode) {
			//192.241.187.135:2100/v1/Zipcode/77001
			return $http({
				url: Endpoint._getEndPoint() + 'Zipcode/' + zipCode,
				method: 'GET',
			});
		}

		function createCampaing(data) {

			return $http({
				url: Endpoint.getVendor() + 'Campaign/' + Endpoint.getSessionId() + '/',
				headers: Endpoint.getGlobalHeaders(),
				method: 'POST',
				data: data,
			});
		}

		function addImageCampaing(campaing_id, file, success, error) {
			var url = Endpoint.getVendor() + 'AddFile/' + localStorage.id + '/';
			var type = 'campaing';

			fileUpload.uploadFileToUrlCampaing(campaing_id, file, url, type, success, error);
		}
		function addTempPdf( file, success, error) {
			var url = Endpoint.getVendor() + 'AddTempFile/' ;
			fileUpload.addTempPdf(file, url,success, error);
		}

		function existsEmail(email) {
			return $http({
				url: Endpoint.getVendor() +"Exists/"+email,
				method: 'GET',
				headers: {'type': 'vendor'},
			});
		}

		function getCategoryList() {
			return $http({
				url: Endpoint._getEndPoint() +"Admin/Categories",
				method: 'GET',
			});
		}



	}]);

})(angular.module("vrumProject.signup"));