(function(module) {

	module.controller('SignupController', ['$scope', 'SignupModelService', '$state', '$sce', 'ShowImageExamples', 'AlertsFactory', '$location', '$filter', function($scope, SignupModelService, $state, $sce, ShowImageExamples, AlertsFactory, $location, $filter) {

		var USER_MESSAGES = {
				// Nombre de los campos de la seccion 'bussiness' para ser mostrados al usuario
				bussiness: {
					name: 'Name',
					address1: 'Address 1',
					city: 'City',
					state: 'State',
					zipCode: 'Zip Code',
					phone: 'Phone',
					hoursOperation: 'Hours of operation',
				},
				// Nombre de los campos de la seccion 'billing' para ser mostrados al usuario
				billing: {
					number: 'Number of the card',
					security: 'Security code of the card',
					expirate: 'Expiration date',
					address: 'Billing address',
					firstName: 'First Name',
					LastName: 'Last Name',
					month: 'Expirate Month',
					year: 'Expirate Year'
				},
				// Nombre de los campos de la seccion 'contact' para ser mostrados al usuario
				contact: {
					name: 'Name',
					lastName: 'Last name',
					phone: 'Phone',
					username: 'Username',
					email: 'Email',
					password: 'Password',
					confirmationPassword: 'Confirmation of password',
				},
				// Nombre de los campos de la seccion 'mhp' para ser mostrados al usuario
				mhp: {
					name: 'Name',
					slogan: 'Slogan',
					description: 'description',
				}
			},

			// Numero de pasos de el registro
			STEPS = 5;

		var model = this,
			stepSelected = 0,
			isSignup = false;
		model.isValidZipCode = true;
		model.validFirstPhoneStep1Bool = true;
		model.validSecondPhoneStep1Bool = true;
		model.validPhoneStep2Bool = true;
		model.nextButtonWasPush = false;

		var skipMhpBool = false;
		var skipSpecialBool = false;

		$scope.imageSrc = '';

		//_init();

		// Metodos expuestos
		model.myFileMhp = 'assets/no_file.png';
		model.isSelected = isSelected;
		model.next = next;
		model.back = back;
		model.skip = skip;
		model.setSelected = setSelected;
		model.isDisabled = isDisabled;
		model.onClickStep = onClickStep;
		model.callServices = callServices;
		$scope.getZipCode = getZipCode;
		model.showImageExamples = showImageExamples;
		model.formatTel = formatTel;

		var boolCompleteSingup = false;

		// Desarrollo de metodos

		// Verifica si el usuario ya creo cuenta y de acuerdo a esto envia a la funcion adecuada para verificar


		// FUncion de binding que verifica si un 'paso' esta desactivado
		function isDisabled(step) {
			//console.log('boolCompleteSingup :', boolCompleteSingup);

			if (stepSelected === 3) {
				boolCompleteSingup = true;
			}
			if (boolCompleteSingup) {
				return false;
			} else {
				if (step === 3 || step === 4) {
					return true;
				}
			}

			var result = preSignupValidate(step - 1);

			if (!result) {
				return false;
			}
			return step > stepSelected;
		}

		// Funcion de binding para verificar si se esa en un 'paso'
		function isSelected(step) {
			//console.log('stepSelected: ', stepSelected);
			if (step === stepSelected) {
				return true;
			} else {
				return false;
			}
		}

		// Funcion para el boton skip
		function skip() {
			if (stepSelected === 2) {
				model.existMHP = false;
				model.myFileMhp = 'assets/no_file.png';
				$scope.imageSrcMhp = 'assets/no_file.png';
				skipMhpBool = true;
				model.mhp.name = '';
				model.mhp.description = '';
				$scope.$broadcast('skipMhp', {
					isPDF: false,
					fileSrc: 'assets/no_file.png',
				});

			} else if (stepSelected === 3) {
				model.myFileSpecial = 'assets/no_file.png';
				$scope.imageSrcSpecial = 'assets/no_file.png';
				skipSpecialBool = true;
				model.nameSpecial = null;
				model.type = null;
				model.datesCorrectBool = true;
				$location.hash('pageTitle');
				$scope.$broadcast('skipSpecial', {
					isPDF: false,
					fileSrc: 'assets/no_file.png',
				});
			}

			stepSelected++;
			model.stepwording = model.WORDINGS[stepSelected];
		}


		// Función cuando el usuario avanza de paso
		function next() {
			//console.log("stepSelected", stepSelected);
			model.nextButtonWasPush = true;

			// llama a la funcion que valida
			var result = preSignupValidate(stepSelected);

			if (stepSelected === 1) {
				if (result) {
					AlertsFactory.show("Hi! Missing some info. Please ensure that all fields have correct data.");
				} else if (model.contact.password !== model.contact.confirmationPassword) {
					AlertsFactory.show("The passwords don`t match.");
					result = true;
				}
			}
			// SI hay errores en el paso
			if (result) {
				if (stepSelected !== 1) {
					AlertsFactory.show("Hi! Missing some info. Please ensure that all fields have correct data.");
				}
			} else {
				model.stepwording = model.WORDINGS[stepSelected + 1];
				// Si no hay erroes se avanza
				if (stepSelected++ >= STEPS) {
					$state.go('login');
				}
			}
			model.nextButtonWasPush = false;
		}

		// Funcion para devolverse de paso
		function back() {
			stepSelected--;
			model.stepwording = model.WORDINGS[stepSelected];
		}

		// funcion para seleccionar un numero de paso especifico
		function setSelected(step) {
			stepSelected = step;
		}

		// Funcion cuando se hace click sobre un tab
		function onClickStep(step) {
			if (isDisabled(step)) {
				return;
			}
			model.stepwording = model.WORDINGS[step];

			setSelected(step);
		}
		

		// Funcion para validar paso 1 VENDOR INFO
		function _validateStep1() {
			if ($scope.vendorInfoStepForm.$valid) {
				if (!model.nextButtonWasPush) {
					return;
				}
				if (model.bussiness.phone.length < 10 || (model.bussiness.phone2 && model.bussiness.phone2.length > 0)) {
					model.validFirstPhoneStep1Bool = (model.bussiness.phone.length < 10) ? false : true;
					var localReturnBool;
					if (model.bussiness.phone2 && model.bussiness.phone2.length > 0) {
						model.validSecondPhoneStep1Bool = (model.bussiness.phone2.length < 10) ? false : true;
						if (model.validFirstPhoneStep1Bool && model.validSecondPhoneStep1Bool) {
							localReturnBool = false;
						} else {
							localReturnBool = true;
						}
					} else {
						localReturnBool = !model.validFirstPhoneStep1Bool;
					}
					return localReturnBool;
				} else {
					model.validFirstPhoneStep1Bool = true;
				}

				return false;
			} else {
				model.validFirstPhoneStep1Bool = true;
				model.validSecondPhoneStep1Bool = true;

				return true;
			}

		}

		// Funcion para validar paso 2 CONTACT INFO
		function _validateStep2() {
			var errors = [];
			if ($scope.contactInformationForm.$valid && model.emailAvailable && model.nextButtonWasPush) {
				model.validPhoneStep2Bool = (model.contact.phone.length < 10) ? false : true;

				return !model.validPhoneStep2Bool;
			} else {
				return true;
			}
		}
		$scope.$watch('validPhoneStep2Bool', function() {
			console.log(model.validPhoneStep2Bool);
		});


		// Funcion para validar paso 3 MHP
		function _validateStep3() {
			return false;
		}

		// Funcion para validar paso 4 CREATE SPECIAL
		function _validateStep4() {
			if (model.type === "info") {
				return false;
			}
			if (new Date(completeDateStart).getTime() < new Date(completeDateFinish).getTime()) {
				model.datesCorrectBool = true;
				$location.hash('pageTitle');
				return false;
			} else {
				model.datesCorrectBool = false;
				return true;
			}
		}

		// Funcion para validar paso 4 Enviar TODO
		function callServices() {
			//Objeto para registro
			var registerData = '';
			var specialData = '';
			var mhpData = '';

			//alert('SE CREO TODO');

			if (!skipSpecialBool) {
				specialData = {
					file: model.myFileSpecial !== 'assets/no_file.png' ? model.myFileSpecial : '',
					data: {
						name: model.nameSpecial,
						content: {
							description: {
								en: model.descriptionEnglish,
								es: '',
							},
						},
						date_start: completeDateStart,
						date_end: completeDateFinish,
						expires: true,
						type: model.type === null ? 'one_time' : model.type,
					},
				};
			}
			if (!skipMhpBool) {
				mhpData = {
					file: model.myFileMhp,
					data: {
						name: model.mhp.name,
						description: model.mhp.description,
					},
				};
			}

			registerData = objectToSend();

			//console.log("REGISTER DATA ",registerData);
			//console.log("MHP DATA ",mhpData);
			//console.log("SPECIAL DATA ",specialData);

			SignupModelService.receiveData(registerData, mhpData, specialData);
		}

		$scope.$on('getFileFromDirective', function(event, args) {
			var type = args.type;
			var data = args.data;
			if (type === 'mhp') {
				model.existMHP = true;
				model.isMhpPDFBool = data.isPDF;
				model.urlMhpPDF = model.isMhpPDFBool ? data.urlPDF : "";
				model.myFileMhp = data.file;
				$scope.imageSrcMhp = (!model.isMhpPDFBool) ? data.imageSrc : null;
				$scope.imageSrcMhpPDF = (model.isMhpPDFBool) ? $sce.trustAsResourceUrl(data.imageSrc) : null;
				$scope.$apply();
				$scope.$broadcast('previewDataMhp', {
					isPDF: model.isMhpPDFBool,
					fileSrc: model.isMhpPDFBool ? data.imageSrc : $scope.imageSrcMhp,
				});

			} else {
				model.isSpecialPDFBool = data.isPDF;
				model.myFileSpecial = data.file;
				model.urlSpecialPDF = model.isSpecialPDFBool ? data.urlPDF : "";
				$scope.imageSrcSpecial = (!model.isSpecialPDFBool) ? data.imageSrc : null;
				$scope.imageSrcSpecialPDF = (model.isSpecialPDFBool) ? $sce.trustAsResourceUrl(data.imageSrc) : null;
				//console.log('DATOSDESDE LA DIRECTIVE', $scope.imageSrcSpecialPDF);
				$scope.$apply();
				$scope.$broadcast('previewDataSpecial', {
					isPDF: model.isSpecialPDFBool,
					fileSrc: model.isSpecialPDFBool ? data.imageSrc : $scope.imageSrcSpecial,
				});
			}
		});

		// Valida si esta activo un campo para ser modificado antes de que se cree el usuario
		function preSignupValidate(step) {
			switch (step) {
				case 0:
					return _validateStep1();
				case 1:
					return _validateStep2();
				case 2:
					return _validateStep3();
				case 3:
					return _validateStep4();
				case 4:
					console.log("MANDAR PERFIL");
					break;
			}
			return true;
		}

		function getZipCode() {
			if (model.bussiness.zipCode.length === 5) {
				SignupModelService.getZipCode(model.bussiness.zipCode, function(city, state, isValid) {
					model.bussiness.city = city;
					model.bussiness.state = state;
					model.isValidZipCode = isValid;
				});
			}
		}
		// Funcion que genera el objeto para enviar al servicio web
		function objectToSend() {
			return {
				email: (model.contact.email).toLowerCase(),
				password: model.contact.password,
				name: model.bussiness.name,
				category: model.category, //no estoy seguro que se envie aca 
				main_address: {
					phone: model.bussiness.phone,
					phone2: model.bussiness.phone2,
					city: model.bussiness.city,
					state: model.bussiness.state,
					zip: model.bussiness.zipCode,
					street1: model.bussiness.address1,
					street2: model.bussiness.address2,
					operation_time: {
						text: model.bussiness.hoursOperation,
					},
				},
				contact_info: {
					name: model.contact.name,
					lastname: model.contact.lastName,
					phone: model.contact.phone,
					phone2: model.contact.phone,
				},
				credit_card: {
					firstname: model.billing.firstName,
					lastname: model.billing.lastName,
					number: model.billing.card.number,
					ccv: model.billing.card.security,
					expire_month: 12,
					expire_year: 21,
				},
				mobile_homepage: {
					/*name: model.mhp.name,
					description: {
						en: model.mhp.description,
					},*/
				},
			};
		}


		/////////////////FUNCTIONS FOR CREATE SPECIAL/////////////////////////////
		var completeDate = "";
		// FUncion para obtener si es meridiano
		$scope.toggleMode = function() {
			$scope.ismeridian = !$scope.ismeridian;
		};
		completeDateStart = "";
		var completeDateStartArraySplit = '';

		$scope.dtStart = new Date();

		// Funcion cuando cambia el tiempo
		$scope.changedTimeStart = function() {
			var currentDate = $scope.mytimeStart.toString().split(" ");
			completeDateStartArraySplit[4] = currentDate[4];
			completeDateStart = completeDateStartArraySplit.toString().replace(/,/g, " ");
			console.log('After TIME START: ', completeDateStart);
		};

		$scope.changedDateStart = function() {
			completeDateStartArraySplit = $scope.mytimeStart.toString().split(" ");
			var currentDate = $scope.dtStart.toString().split(" ");
			for (var i = 0; i <= 3; i++) {
				completeDateStartArraySplit[i] = currentDate[i];
			}
			completeDateStart = completeDateStartArraySplit.toString().replace(/,/g, " ");
			console.log('AfterDATE START: ', completeDateStart);
		};
		// Funcion para abrir el datetime-picker
		$scope.openStart = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.openedStart = true;
		};

		completeDateFinish = "";
		var completeDateFinishArraySplit = '';

		$scope.dtFinish = new Date();

		// Funcion cuando cambia el tiempo
		$scope.changedTimeFinish = function() {

			var currentDate = $scope.mytimeFinish.toString().split(" ");
			completeDateFinishArraySplit[4] = currentDate[4];
			completeDateFinish = completeDateFinishArraySplit.toString().replace(/,/g, " ");
			console.log('After TIME FINISH: ', completeDateFinish);
		};

		$scope.changedDateFinish = function() {
			completeDateFinishArraySplit = $scope.mytimeFinish.toString().split(" ");
			var currentDate = $scope.dtFinish.toString().split(" ");
			for (var i = 0; i <= 3; i++) {
				completeDateFinishArraySplit[i] = currentDate[i];
			}
			completeDateFinish = completeDateFinishArraySplit.toString().replace(/,/g, " ");
			console.log('AfterDATE FINISH: ', completeDateFinish);
		};

		// Funcion para abrir el datetime-picker
		$scope.openFinish = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.openedFinish = true;
		};

		function ispdfFunction(param) {
			console.log('EL PARAEMTRO DENTRO DE IS PDF', param);
			if (~param.indexOf('.pdf')) {
				return true;
			} else {
				return false;
			}

		}

		function showImageExamples(type) {
			ShowImageExamples.show(type);
		}
		$scope.$watch('model.emailFocus', function() {

			console.log('hey, myVar has changed!', model.emailFocus);
			if (!model.emailFocus) {
				SignupModelService.existsEmail(model.contact.email, function(value) {
					model.emailAvailable = value;
				});
			}
		});

		function getCategoryList() {
			model.categoryList = {};
			SignupModelService.getCategoryList(function(data) {
				model.categoryList = data;
				console.log("model.categoryList", model.categoryList);
			});

		}

		function formatTel(number, type) {
			model.contact.phone = (type === "model.contact.phone") ? $filter('tel')(number) : model.contact.phone;
			model.contact.phone2 = (type === "model.contact.phone2") ? $filter('tel')(number) : model.contact.phone2;
			model.bussiness.phone = (type === "model.bussiness.phone") ? $filter('tel')(number) : model.bussiness.phone;
			model.bussiness.phone2 = (type === "model.bussiness.phone2") ? $filter('tel')(number) : model.bussiness.phone2;
		}

		function _init() {
			getCategoryList();

			model.existMHP = false;
			model.specialWasCreatedBool = true;

			model.emailAvailable = true;
			model.isMhpPDFBool = false;
			model.isSpecialPDFBool = false;
			model.WORDINGS = ['Sign Up Now! Your customers are looking for you on VRüM!',
				'Get your mobile presence today and broaden your market reach.',
				'Create your mobile presence!',
				' Create a special.',
				'Quit aiming at a moving target: Be the target!',
			];
			model.emailFocus = true;
			$scope.blur = false;

			model.myFileSpecial = 'assets/no_file.png';
			$scope.imageSrcMhp = '';
			$scope.imageSrcSpecial = '';
			model.nameSpecial = null;

			model.stepwording = model.WORDINGS[stepSelected];
			model.bussiness = {
				name: '',
				address1: '',
				address2: '',
				city: '',
				state: '',
				zipCode: '',
				phone: '',
				//hoursOperation: '',
			};
			model.billing = {
				card: {
					number: '',
					security: '',
					expirate: {
						month: '',
						year: '',
					},
				},
				address: '',
				firstName: '',
				lastName: ''
			};
			model.contact = {
				name: '',
				lastName: '',
				email: '',
				phone: '',
				phone2: '',
				password: '',
				confirmationPassword: '',
			};

			model.mhp = {
				name: 'd',
				description: 'd',
			};

			////////////////////// SPECIALS////////////
			model.datesCorrectBool = true;
			$scope.formats = ['dd/MMM/yyyy ', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.ismeridian = true;
			$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};

			$scope.hstep = 1;
			$scope.mstep = 1;
			$scope.mytimeStart = new Date();
			$scope.mytimeFinish = new Date();

			$scope.options = {
				hstep: [1, 2, 3],
				mstep: [1, 5, 10, 15, 25, 30]
			};
			$scope.ismeridian = true;
			$scope.changedDateFinish();
			$scope.changedDateStart();
			$scope.expires = true;
			////////////////////// SPECIALS////////////
		}
		_init();
	}]);
	module.filter('tel', function() {
		return function(tel) {
			if (!tel) {
				return '';
			}

			var value = tel.toString().trim().replace(/^\+/, '');

			if (value.match(/[^0-9]/)) {
				return tel;
			}

			var country, city, number;

			switch (value.length) {
				case 10: // +1PPP####### -> C (PPP) ###-####
					country = 1;
					city = value.slice(0, 3);
					number = value.slice(3);
					break;

				case 11: // +CPPP####### -> CCC (PP) ###-####
					country = value[0];
					city = value.slice(1, 4);
					number = value.slice(4);
					break;

				case 12: // +CCCPP####### -> CCC (PP) ###-####
					country = value.slice(0, 3);
					city = value.slice(3, 5);
					number = value.slice(5);
					break;

				default:
					return tel;
			}

			if (country == 1) {
				country = "";
			}

			number = number.slice(0, 3) + '-' + number.slice(3);

			return (country + " (" + city + ") " + number).trim();
		};
	});

}(angular.module("vrumProject.signup")));