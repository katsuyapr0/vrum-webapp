(function(module) {

  function DashboardModelService($http, DashboardWebServices, Endpoint, $state) {
    var _self = this;

    // Funcion expuesta
    _self.init = init;
    _self.updateVendorInfo = updateVendorInfo;
    _self.getVrumStats=getVrumStats;


    // Desarrollo de funcion
    function init(save) {
      // Funcion que se activa cuando obtiene correctamente la informacion del vendedor
      function success(data) {
        if (data.status) {
          Endpoint.setUserInfoSingleton(data.response);
          console.log('se obtuvieron correctamente los datos del vendor', data);
        } else {
          console.log('No se pudo obtener los datos del vendor', data);
         // AlertsFactory.show("The user data can't be bring");
        }

        var user = data.response;

        function successActiveCampaigns(data) {
          console.log("Campañas activas del vendor", data);
          user.campaigns = data.response;
          save(user, true);
        }

        function errorActiveCampaigns(data) {
         // Endpoint.notConnectionAlert();
          console.log(data);
          save(user, true);
        }
        //Inactive Specials
        function successInactiveCampaigns(data) {
          console.log("Campañas INactivas del vendor", data);
          user.inactive_campaigns = data.response;
          save(user, false);
        }

        function errorInactiveCampaigns(data) {
          //Endpoint.notConnectionAlert();
          console.log(data);
          save(user, false);
        }

        // Llama al servicio para obtener las campañas activas del Vendor
        DashboardWebServices.getVendorInactiveCampaings().success(successInactiveCampaigns).error(errorInactiveCampaigns);
        setTimeout(function() {
          DashboardWebServices.getVendorActiveCampaings().success(successActiveCampaigns).error(errorActiveCampaigns);
        }, 1000);


      }

      // Funcion que se activa cuando no pudo obtener la información del Vendor
      function error(data) {
        Endpoint.notConnectionAlert();
        console.log(data);
        // TODO: Show a message to user
      }
      // Llama al servicio para obtener la informacion del Vendor
      DashboardWebServices.getVendorInfo().success(success).error(error);
    }

    function getVrumStats(myFunction) {

      function success(data) {
        if (data.status) {
          myFunction(data);
          console.log(data);
        }
      }
      function error(data) {
        //Endpoint.notConnectionAlert();
      }
      DashboardWebServices.getVrumStats().success(success).error(error);
    }

    function addImageVendor(file) {
      DashboardWebServices.addImageVendor(file, function(data) {
        console.log(" SE PUDO ENVIAR", data);
      }, function(data) {
        //Endpoint.notConnectionAlert();
        console.log("NO SE PUDO ENVIAR", data);
      });
    }


    function updateVendorInfo(name, lastname, phone, address1, address2, city, state, zipCode, hoursOperation, lastScope, myFile) {
      //      console.log('UPDATE VENDOR FUNCTION', data);

      function success(data) {
        if (data.status) {
          addImageVendor(myFile);
          lastScope.showVendorInfo = false;

          //alert('The vendor was update');
        } else {
          alert('The vendor could not be updated');
        }
        console.log("El vendor fue actualizado", data);
      }

      function error(data) {
        //Endpoint.notConnectionAlert();
      }
      // Llama el servicio para autenticarse
      DashboardWebServices.updateVendorInfo(name, lastname, phone, address1, address2, city, state, zipCode, hoursOperation).success(success).error(error);
    }


  }

  module.service('DashboardModelService', [
    '$http',
    'DashboardWebServices',
    'Endpoint',
    '$state',
    DashboardModelService
  ]);

}(angular.module("vrumProject.dashboard")));