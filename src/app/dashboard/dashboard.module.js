(function(module) {

  module.config(function($stateProvider) {
    // COnfiguración de la ruta
    $stateProvider.state('dashboard', {
      url: '/dashboard',
      views: {
        "main": {
          controller: 'DashboardController as model',
          templateUrl: 'dashboard/dashboard.tpl.html'
        }
      },
      data: {
        pageTitle: 'Dashboard'
      }
    });
  });

}(angular.module("vrumProject.dashboard", [
  'ui.router'
])));