(function(module) {

  module.service('DashboardWebServices', ['$http', 'Endpoint', 'fileUpload', function($http, Endpoint, fileUpload) {
    var _self = this;

    // Metodos expuestos
    _self.getVendorInfo = getVendorInfo;
    _self.getVendorActiveCampaings = getVendorActiveCampaings;
    _self.getVendorInactiveCampaings = getVendorInactiveCampaings;
    _self.updateVendorInfo = updateVendorInfo;
    _self.addImageVendor = addImageVendor;
    _self.getVrumStats=getVrumStats;

    // Desarrollo de funciones 

    function addImageVendor(file, success, error) {
      var url = Endpoint.getVendor() + 'AddFile/' + localStorage.id + '/',
        type = 'mhp';

      fileUpload.uploadFileToUrlVendor(file, url, type, success, error);
    }

    // Funcion para obtener la informacion del vendedor
    function getVendorInfo() {
      return $http({
        url: Endpoint.getVendor() + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        //headers: Endpoint.getExpiredHeaders(), // Only for testing expired session
        method: 'GET',
      });
    }

    // Funcion para obtener las campañas activas del Vendor
    function getVendorActiveCampaings() {
      console.log('endpoint', Endpoint.getVendor() + 'Campaigns/Active/' + Endpoint.getSessionId() + '/');
      return $http({
        url: Endpoint.getVendor() + 'Campaigns/Active/' + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'GET',
      });
    }

    function getVendorInactiveCampaings() {
      console.log('endpoint', Endpoint.getVendor() + 'Campaigns/Active/' + Endpoint.getSessionId() + '/');
      return $http({
        url: Endpoint.getVendor() + 'Campaigns/Inactive/' + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'GET',
      });
    }

    function updateVendorInfo(name, lastname, phone, address1, address2, city, state, zipCode, hoursOperation) {

      console.log('updateVendorInfo web services', Endpoint.getVendor() + Endpoint.getSessionId());
      return $http({
        url: Endpoint.getVendor() + Endpoint.getSessionId() + '/',
        headers: Endpoint.getGlobalHeaders(),
        method: 'PUT',
        data: {
          contact_info: {
            name: name,
            lastname: lastname,
            phone: phone,
          },
          main_address: {
            street1: address1,
            street2: address2,
            city: city,
            state: state,
            zip: zipCode,
            operation_time: {
              text: hoursOperation
            },
          },
        },
      });
    }

    function getVrumStats() {
      return $http({
      //  http://192.241.187.135:2100/v1/Vendor/VrumStats/
      //  http://192.241.187.135:2100/v1/
        url: Endpoint._getEndPoint() +  'Vendor/VrumStats/' ,
        headers: Endpoint.getGlobalHeaders(),
        method: 'GET',
      });
    }

  }]);

})(angular.module("vrumProject.dashboard"));