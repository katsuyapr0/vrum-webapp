(function(module) {

	module.controller('DashboardController', ['DashboardModelService', 'CampaignDialog', '$scope', '$sce', 'UpdateVendorInfo', 'Endpoint', 'ChangePassword', 'EditMHP', '$state', 'updateDashBoardService', function(DashboardModelService, CampaignDialog, $scope, $sce, UpdateVendorInfo, Endpoint, ChangePassword, EditMHP, $state, updateDashBoardService) {
		var model = this;
		// Vendor del dashboard
		model.vendor = {};
		model.imageSrc = '';
		model.isPDFBool = false;
		model.imageSrcPDF = '';

		// Campañas del Vendor
		model.campaigns = [];
		// Funcion para añadir campaña
		model.addCampaign = addCampaign;
		model.updateVendorInfo = updateVendorInfo;
		model.changePassword = changePassword;
		model.timeRemaining = timeRemaining;
		model.editMHP = editMHP;
		model.editCampaign = editCampaign;
		model.logout = logout;
		model.setUserInfoSingleton = setUserInfoSingleton;
		model.goToPayments = goToPayments;

		init();

		function timeRemaining(date) {
			//Get 1 day in milliseconds
			var one_month = 30 * 24 * 60 * 60 * 1000;
			var one_day = 24 * 60 * 60 * 1000;
			var one_hour = 24 * 60 * 1000;

			var date_1 = new Date(date);
			var nowDate = new Date();
			var leftVar = '';
			var months = '';
			var days = '';
			var hours = '';

			var leftTime = date_1.getTime() - nowDate.getTime();

			if (leftTime > one_month) {
				months = parseInt(leftTime / one_month);
				leftVar = leftTime % one_month;
				if (leftVar > one_day) {
					days = parseInt(leftVar / one_day);
					leftVar = leftVar % one_day;
				}
			} else if (leftTime > one_day) {
				days = parseInt(leftTime / one_day);
				leftVar = leftVar % one_day;
				if (leftVar > one_hour) {
					hours = parseInt(leftVar / one_hour);
					leftVar = leftVar % one_hour;
				}
			} else if (leftTime > one_hour) {
				hours = parseInt(leftVar / one_hour);
				leftVar = leftVar % one_hour;
			}

			var returnValue = '';
			if (months >= 1) {
				if (months > 1) {
					returnValue = months + ' months';
				} else {
					returnValue = months + ' months and ' + days + ' days';
				}
			} else if (days >= 2) {
				returnValue = days + ' days';
			} else {
				if (days >= 1) {
					returnValue = days + ' days and ' + hours + ' hours';
				} else {
					returnValue = hours + ' hours';
				}
			}

			return returnValue;
		}

		function goToPayments() {
			$state.go('payments');
		}


		function addCampaign() {
			CampaignDialog.show(model);
		}

		function updateVendorInfo() {
			UpdateVendorInfo.show(model);
		}

		function editMHP() {
			EditMHP.show(model);
		}

		function changePassword() {
			ChangePassword.show();
		}

		function setUserInfoSingleton() {
			Endpoint.setUserInfoSingleton(model.vendor);
		}

		function editCampaign(campaign) {
			Endpoint.setMyCampaign(campaign);
			CampaignDialog.show(model);
		}

		function logout() {
			localStorage.removeItem('messageToAlert');
			localStorage.removeItem('token');
			localStorage.removeItem('id');
			localStorage.removeItem('email');
			localStorage.removeItem('password');
			localStorage.removeItem('remember');
			localStorage.removeItem('isLogin');
			localStorage.clear();
			$state.go('login');
		}

		function init() {
			//model.sendBroadcast = false;
			updateDashBoardService.updateDashboard(model);

		}

		$scope.$watch('model.vendor', function() {
			$scope.$broadcast('previewDataMhp', {
				isPDF: model.isPDFBool,
				fileSrc: model.vendor.mobile_homepage ? model.vendor.mobile_homepage.page_file.url : "",
			});
		});



	}]);

	module.service('updateDashBoardService', ['DashboardModelService', '$sce', function(DashboardModelService, $sce) {

		var model = this;

		//var isPDFBoolLOCAL;
		//var imageSrcLOCAL;
		model.updateDashboard = updateDashboard;
		//model.getParams = getParams;

		function updateDashboard(lastScope) {
			//lastScope.reload();

			//console.log('updateDashboard', lastScope);
			//console.log('LA VAR',lastScope.model.hola);

			lastScope.progressValue = 25;
			lastScope.haveMHPBool = true;
			lastScope.haveCampaigns = false;
			lastScope.haveActiveCampaignsBool = false;
			lastScope.haveInactiveCampaignsBool = false;
			lastScope.showProgressBarBool = true;
			lastScope.isPDFBool = !lastScope.isPDFBool;
			lastScope.VRUM_STATS = "";

			DashboardModelService.getVrumStats(function(data) {
				lastScope.VRUM_STATS = data.response;
				//console.log("VRUM_STATS", lastScope.VRUM_STATS );
			});

			// Llama el servicio de inicialización del modelo del dashboard y asigna valores luego de hacer todo su proceso

			DashboardModelService.init(function(vendor, verifyCampaignsMhpBool) {
				lastScope.progressValue = 25;
				lastScope.vendor = vendor;
				console.log("vendor", lastScope.vendor);
				lastScope.campaigns = vendor.campaigns;
				lastScope.inactive_campaigns = vendor.inactive_campaigns;

				if (vendor.mobile_homepage) {
					lastScope.imageSrc = vendor.mobile_homepage.page_file.url;
				}
				lastScope.isPDFBool = (~lastScope.imageSrc.indexOf('.pdf')) ? true : false;
				lastScope.imageSrcPDF = (~lastScope.imageSrc.indexOf('.pdf')) ? $sce.trustAsResourceUrl(lastScope.imageSrc) : null;

				lastScope.imageSrc = (~lastScope.imageSrc.indexOf('.pdf')) ? null : lastScope.imageSrc;
				//console.log("NEW VENDOR", lastScope.vendor);
				lastScope.setUserInfoSingleton();

				if (vendor.stripe && vendor.stripe.is_active) {
					lastScope.progressValue += 25;
					lastScope.havePayment = true;
				} else {
					lastScope.havePayment = false;
				}

				//console.log("inactive_campaigns", vendor.inactive_campaigns);
				if (verifyCampaignsMhpBool) {
					if (vendor.mobile_homepage) {
						lastScope.progressValue += 25;
					}
					if (vendor.campaigns['length'] !== 0 || vendor.inactive_campaigns['length'] !== 0) {
						lastScope.progressValue += 25;
					}
					lastScope.haveActiveCampaignsBool = (vendor.campaigns['length'] !== 0) ? true : false;
					lastScope.haveInactiveCampaignsBool = (vendor.inactive_campaigns['length'] !== 0) ? true : false;

					lastScope.haveCampaigns = (lastScope.haveActiveCampaignsBool || lastScope.haveInactiveCampaignsBool) ? true : false;
				}

				lastScope.showProgressBarBool = (lastScope.progressValue !== 100) ? true : false;
				//imageSrcLOCAL = vendor.mobile_homepage.page_file.url;
				//isPDFBoolLOCAL = (~imageSrcLOCAL.indexOf('.pdf')) ? true : false;
				lastScope.sendBroadcast = true;

			});
		}
		/*
				function getParams() {
					var obj = {
						isPdf: isPDFBoolLOCAL,
						imageSrc: imageSrcLOCAL,
					};
					return obj;
				}
		*/


	}]);

}(angular.module("vrumProject.dashboard")));